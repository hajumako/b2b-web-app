<?php

/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 25.09.2017
 * Time: 12:21
 */
class getSwaggerDocs
{
  const DEV = 'dev';
  const BETA = 'beta';
  const LOCAL = 'local';
  const PROD = 'prod';

  const ORIGINS = [
    self::DEV => 'http://api.terma24.local/docs',
    self::LOCAL => 'http://api.terma.local/docs',
    self::BETA => 'https://beta-api.terma24.pl/docs',
    self::PROD => 'http://api.terma24.pl/docs'
  ];

  const DST = [
    self::DEV => 'dev_api.json',
    self::LOCAL => 'local_api.json',
    self::BETA => 'beta_api.json',
    self::PROD => 'prod_api.json'
  ];

  private $contents = '';

  /**
   * swagger docs getter, source depending on origin type (dev, beta, prod)
   * getSwaggerDocs constructor.
   * @param string $type - docs source type (dev, beta, prod)
   */
  public function __construct($type = self::DEV)
  {
    $origin = self::ORIGINS[$type];
    echo 'docs origin: ' . $origin;
    if($origin) {
      $this->contents = file_get_contents($origin);
      file_put_contents(self::DST[$type], $this->contents);
    }
  }
}

if(count($argv) > 1) {
  new getSwaggerDocs($argv[1]);
} else {
  new getSwaggerDocs();
}
