@echo off

rd /Q /S ..\src\app\api

set executable=.\swagger-codegen-cli.jar

php%2 getSwaggerDocs.php %1

If Not Exist %executable% (
  mvn clean package
)

set source=dev_api.json

if [%1]==[] (
  GOTO BLANK
)
if %1 == beta (
  set source=beta_api.json
)
if %1 == local (
  set source=local_api.json
)
if %1 == prod (
  set source=prod_api.json
)

:BLANK

REM set JAVA_OPTS=%JAVA_OPTS% -Xmx1024M
set ags=generate -i %source% -l typescript-angular2 -o ../src/app/api

@echo on

java %JAVA_OPTS% -jar %executable% %ags%
