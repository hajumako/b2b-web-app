#!/bin/bash

rm -rf ../src/app/api

executable="swagger-codegen-cli.jar"

php getSwaggerDocs.php $1

if [ ! -f $executable ]; then
    mvn clean package
fi

source="dev_api.json"

if [ -n "$1" ]; then
    if [ $1 = "beta" ]; then
        source="beta_api.json"
        fi
    if [ %1 = "local" ]; then
        source="local_api.json"
    fi
    if [ %1 = "prod" ]; then
        source="prod_api.json"
    fi
fi

JAVA_OPTS=" -Xmx1024M"
ags="generate -i $source -l typescript-angular2 -o ../src/app/api"

java $JAVA_OPTS -jar $executable $ags