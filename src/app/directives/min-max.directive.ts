import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[MinMax]'
})
export class MinMaxDirective {

  @Input() public min: number = NaN;
  @Input() public max: number = NaN;

  constructor(private el: ElementRef) {
  }

  @HostListener('keyup', ['$event'])
  public onKeyUp(event) {
    let v = this.el.nativeElement.value;
    if (v !== '') {
      if (!isNaN(this.min) && <number> v < this.min) {
        this.el.nativeElement.value = this.min;
      }
      if (!isNaN(this.max) && <number> v > this.max) {
        this.el.nativeElement.value = this.max;
      }
    }
  }

  @HostListener('blur', ['$event'])
  public onBlur(event) {
    let v = this.el.nativeElement.value;
    if (!isNaN(this.min) && v === '' && <number> v < this.min) {
      this.el.nativeElement.value = this.min;
    }
  }
}
