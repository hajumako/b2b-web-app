import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/takeWhile';

@Injectable()
export class EventEmitterService {

  public static isJsonString(str: string): boolean {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  public static isOfType(str: string, type: string): boolean {
    if (str) {
      try {
        let obj = JSON.parse(str);
        if (obj.hasOwnProperty('type') && obj.type === type) {
          return true;
        }
      } catch (e) {
        // do nothing
      }
    }
    return false;
  }

  public readonly CONFIGURATOR: string = 'configurator';
  public readonly ORDER: string = 'order';
  public readonly PARSER: string = 'parser';

  public event: Observable<string>;

  private _eventSource = new BehaviorSubject<string>(null);

  constructor() {
    this.event = this._eventSource.asObservable();
  }

  public emitEvent(code: string): void {
    this.emit(code);
  }

  public error(error: object): void {
    this.emit(JSON.stringify({error}));
  }

  public info(info: object): void {
    this.emit(JSON.stringify({info}));
  }

  private emit(data: string): void {
    this._eventSource.next(data);

    // behaviorsubject returns the last value on new subscr
    // this ensures that emited event won't be read once again
    setTimeout(() => {
      this._eventSource.next('clear');
    }, 100);
  }
}
