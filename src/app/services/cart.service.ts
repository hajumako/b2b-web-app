import { Injectable, InjectionToken, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { EventEmitterService } from './event-emitter.service';
import { CountriesCacheModel } from '../models/countries-cache.model';
import { CacheService } from './cache.service';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import {
  DefaultService,
  Order,
  LineItem,
  ResponseCode,
  OrderStatusInterface,
  DeliveryTypeInterface,
  Address,
  PaymentMethodInterface,
  Payment,
  OrderStatus,
  LogisticsAddressCountryRegionContract,
  PriceCustomerResponseTER,
  JsonObj,
  ProductConfiguration
} from '../api';
import 'rxjs/add/operator/takeWhile';

const CACHE_CART = new InjectionToken<string>('cache_cart');
const CACHE_CONFIGS = new InjectionToken<string>('cache_configs');
const CACHE_PAYMENT = new InjectionToken<string>('cache_payment');
const CACHE_ORDER_DATA = new InjectionToken<string>('cache_order_data');
const CACHE_COUNTRIES = new InjectionToken<string>('cache_countries_table');

@Injectable()
export class CartService implements OnDestroy {
  public cartCnt: number = 0;
  public sending: boolean = false;
  public countries: LogisticsAddressCountryRegionContract[];

  private _cart: Order;
  private _configDatas: Object = {};
  private _payment: Payment;
  private _additionalData: Object = {};
  private _alive: boolean = true;

  constructor(private API: DefaultService,
              private cacheService: CacheService,
              private authService: AuthService,
              private eventEmitter: EventEmitterService) {
    let cCart = sessionStorage.getItem(CACHE_CART.toString());
    if (cCart) {
      this._cart = JSON.parse(cCart);
      this.countLineItems();
    }

    let cConfigs = localStorage.getItem(CACHE_CONFIGS.toString());
    if (cConfigs) {
      this._configDatas = JSON.parse(cConfigs);
    }

    let cPayment = sessionStorage.getItem(CACHE_PAYMENT.toString());
    if (cPayment) {
      this._payment = JSON.parse(cPayment);
    }

    let cAdditionalData = sessionStorage.getItem(CACHE_ORDER_DATA.toString());
    if (cAdditionalData) {
      this._additionalData = JSON.parse(cAdditionalData);
    }

    this.authService.isLoggedInObs
      .takeWhile(() => this._alive)
      .subscribe(
        loggedIn => {
          if (loggedIn) {
            this.APIgetOrder();
          }
        }
      );
  }

  public getCart(): Order {
    return this._cart;
  }

  public clearCart() {
    this._cart.lineItems.forEach(item => {
      item.deleted = 1;
    });
    this.APIsetLineItems();
    this.countLineItems();
    this.calculateCart();
    sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
  }

  public refreshCart() {
    this.cartCnt = 0;
    this._cart = null;
    this.APIgetOrder();
  }

  public getAdditionalData(key?: string): JsonObj {
    if (key) {
      if (this._additionalData.hasOwnProperty(key)) {
        return this._additionalData[key];
      }
      return null;
    }
    return this._additionalData;
  }

  public getPayment(): Payment {
    return this._payment;
  }

  public getLineItems(): LineItem[] {
    let skus = [];
    this._cart.lineItems.forEach((item, i) => {
      skus.push(item.sku);
    });
    this.updatePrices(skus);
    return this._cart.lineItems;
  }

  public getCartSkus(): string[] {
    let skus = [];
    this._cart.lineItems.forEach(lineItem => {
      skus.push(lineItem.sku);
    });
    return skus;
  }

  public addToCart(cartItem: LineItem): LineItem[] {
    if (!this.inCart(cartItem)) {
      this._cart.lineItems.push(cartItem);
      this.cartCnt++;
    }
    this.APIsetLineItems();
    this.countLineItems();
    this.calculateCart();
    sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
    return this._cart.lineItems;
  }

  public addToCartBulk(cartItems: LineItem[]) {
    cartItems.forEach(cartItem => {
      if (!this.inCart(cartItem)) {
        this._cart.lineItems.push(cartItem);
        this.cartCnt++;
      }
    });
    this.APIsetLineItems();
    this.countLineItems();
    this.calculateCart();
    sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
  }

  public removeFromCart(i: number): LineItem[] {
    this._cart.lineItems[i].deleted = 1;
    this.APIsetLineItems();
    this.countLineItems();
    this.calculateCart();
    sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
    return this._cart.lineItems;
  }

  public getConfigs(skus: string[]): Observable<Object> {
    let observable;
    let foundConfigs = [];
    let missingConfigs = [];
    skus.forEach(sku => {
      if (this._configDatas.hasOwnProperty(sku)) {
        foundConfigs[sku] = this._configDatas[sku];
      } else {
        missingConfigs.push(sku);
      }
    });
    if (missingConfigs.length) {
      observable = this.API.configuratorConfigsGetPostWithHttpInfo(missingConfigs, AuthService.getRequestParams())
        .map(
          (response: Response) => {
            if (response.status === 204) {
              return undefined;
            } else {
              console.log('configuratorConfigsGet', response);
              (<ProductConfiguration[]> response.json()).forEach(configData => {
                this._configDatas[configData.sku] = configData;
                foundConfigs[configData.sku] = configData;
              });
              localStorage.setItem(CACHE_CONFIGS.toString(), JSON.stringify(this._configDatas));
              return foundConfigs;
            }
          }
        );
    } else {
      observable = Observable.of(foundConfigs);
    }
    return observable;
  }

  public getConfig(sku: string): Observable<ProductConfiguration> {
    let observable;
    if (this._configDatas.hasOwnProperty(sku)) {
      observable = Observable.of(this._configDatas[sku]);
    } else {
      observable = this.API.configuratorConfigGetSkuGetWithHttpInfo(sku, AuthService.getRequestParams())
        .map(
          (response: Response) => {
            if (response.status === 204) {
              return undefined;
            } else {
              console.log('configuratorConfigGetSku', response);
              this._configDatas[sku] = <ProductConfiguration> response.json();
              localStorage.setItem(CACHE_CONFIGS.toString(), JSON.stringify(this._configDatas));
              return this._configDatas[sku];
            }
          }
        );
    }
    return observable;
  }

  public setConfig(configData: ProductConfiguration) {
    if (configData !== undefined) {
      this._configDatas[configData.sku] = configData;
      localStorage.setItem(CACHE_CONFIGS.toString(), JSON.stringify(this._configDatas));
      this.APIsetConfiguration(configData);
    }
  }

  public calculateCart() {
    let amount = 0;
    for (let item of this._cart.lineItems) {
      if (!(<number> item.deleted)) {
        amount += item.amount * item.quantity;
      }
    }
    this._cart.amount = amount;
  }

  public updateOrderStatus(status: OrderStatusInterface) {
    this.APIsetOrderStatus(status);
  }

  public setAddresses(addresses: Address[]) {
    this._cart.addresses = addresses;
    this.APIsetAddresses();
  }

  public setDeliveryType(deliveryType: DeliveryTypeInterface) {
    this._cart.deliveryType = deliveryType;
    this.APIsetDeliveryType();
  }

  public setAdditionalData(additionalData: JsonObj) {
    for (let prop in additionalData) {
      if (additionalData.hasOwnProperty(prop)) {
        this._additionalData[prop] = additionalData[prop];
      }
    }
    this.APIsetAdditionalData();
  }

  public setPaymentMethod(transactionId: number, paymentMethod: PaymentMethodInterface) {
    this._payment.transactionId = transactionId;
    this._payment.paymentMethod = paymentMethod;
    this.APIsetPaymentMethod();
  }

  public fillWithDefault() {
    this.API.orderOrderIdDefaultdataPostWithHttpInfo(this._cart.orderId, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdDefaultdata', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._cart = <Order> data.json();
            this.countLineItems();
            sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
            this.APIgetPaymentMethod();
            this.APIgetAdditionalData();
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  public getCountries() {
    let countriesCache = <CountriesCacheModel> this.cacheService.getCache(CACHE_COUNTRIES.toString());
    if (countriesCache !== null) {
      if (countriesCache.expires - new Date().getTime() > 0) {
        this.countries = countriesCache.countries;
        return;
      }
    }
    this.APIgetCountries();
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  private inCart(cartItem: LineItem): boolean {
    let inCart = false;
    for (let item of this._cart.lineItems) {
      if (item.sku === cartItem.sku) {
        if ((<number> item.deleted) === 1) {
          item.deleted = 0;
          item.quantity = cartItem.quantity;
        } else {
          item.quantity += cartItem.quantity;
        }
        inCart = true;
        break;
      }
    }

    return inCart;
  }

  private APIgetOrder() {
    this.API.orderCartGetGetWithHttpInfo(AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderCartGet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._cart = <Order> data.json();
            this.countLineItems();
            sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
            this.APIgetPaymentMethod();
            this.APIgetAdditionalData();
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIgetPaymentMethod() {
    this.API.orderOrderIdPaymentmethodGetGetWithHttpInfo(this._cart.orderId, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdPaymentmethodGet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._payment = <Payment> data.json();
            sessionStorage.setItem(CACHE_PAYMENT.toString(), JSON.stringify(this._payment));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIgetAdditionalData() {
    this.API.orderOrderIdDataGetGetWithHttpInfo(this._cart.orderId, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdDataGet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._additionalData = <JsonObj> data.json();
            sessionStorage.setItem(CACHE_ORDER_DATA.toString(), JSON.stringify(this._additionalData));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetAdditionalData() {
    this.API.orderOrderIdDataSetPostWithHttpInfo(this._cart.orderId, this._additionalData, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdDataSet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._additionalData = <JsonObj> data.json();
            sessionStorage.setItem(CACHE_ORDER_DATA.toString(), JSON.stringify(this._additionalData));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetAddresses() {
    this.API.orderOrderIdAddressesSetPostWithHttpInfo(this._cart.orderId, this._cart.addresses, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdAddressesSet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._cart.addresses = <Address[]> data.json();
            sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetDeliveryType() {
    this.API.orderOrderIdDeliverytypeSetPostWithHttpInfo(this._cart.orderId, {deliveryType: this._cart.deliveryType}, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdDeliverytypeSet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._cart.deliveryType = <DeliveryTypeInterface> data.json().deliveryType;
            sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetPaymentMethod() {
    this.API.orderOrderIdPaymentmethodSetPostWithHttpInfo(this._cart.orderId, {
      transactionId: this._payment.transactionId,
      paymentMethod: this._payment.paymentMethod
    }, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdPaymentmethodSet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            let payment = <Payment> data.json();
            this._payment.transactionId = payment.transactionId;
            this._payment.paymentMethod = payment.paymentMethod;
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetLineItems() {
    this.sending = true;
    this.API.orderOrderIdLineitemsSetPostWithHttpInfo(this._cart.orderId, this._cart.lineItems, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdLineitemsSet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._cart.lineItems = <LineItem[]> data.json();
            this.countLineItems();
            sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
          }
          this.sending = false;
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetOrderStatus(status: OrderStatusInterface) {
    this.API.orderOrderIdStatusSetPostWithHttpInfo(this._cart.orderId, {status}, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('orderOrderIdStatusSet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            let orderStatus = <OrderStatus> data.json();
            this.eventEmitter.emitEvent(JSON.stringify({orderStatusUpdated: orderStatus.status}));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIgetCountries() {
    this.API.countriesListGetWithHttpInfo(AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('countriesList', data);
          this.countries = <LogisticsAddressCountryRegionContract[]> data.json();
          let colors: CountriesCacheModel = {
            expires: new Date().getTime() + (CACHE_EXP * 24 * 60 * 60 * 1000),
            countries: this.countries
          };
          this.cacheService.saveCache(CACHE_COUNTRIES.toString(), colors);
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private APIsetConfiguration(configData: ProductConfiguration) {
    this.API.configuratorConfigSetSkuPostWithHttpInfo(configData.sku, configData, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('configuratorConfigSetSku', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this._configDatas[configData.sku] = configData;
            localStorage.setItem(CACHE_CONFIGS.toString(), JSON.stringify(this._configDatas));
          }
        },
        error => {
          // emitt event that informs app about error & displays modal
          this.eventEmitter.error(error);
        }
      );
  }

  private updatePrices(skus: string[]) {
    this.API.configuratorPricesGetPostWithHttpInfo(skus, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('configuratorPricesGet', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            (<PriceCustomerResponseTER[]> data.json()).forEach(priceData => {
              let i = skus.indexOf(priceData.itemId + '-' + priceData.configId);
              this._cart.lineItems[i].amount = priceData.lineAmount;
            });
            this.calculateCart();
            sessionStorage.setItem(CACHE_CART.toString(), JSON.stringify(this._cart));
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private countLineItems() {
    if (this._cart.lineItems != null) {
      let cnt = this._cart.lineItems.length;
      this._cart.lineItems.forEach(li => {
        cnt -= li.deleted;
      });
      this.cartCnt = cnt;
    }
  }
}
