import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Headers } from '@angular/http';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/take';
import 'rxjs/add/observable/timer';
import supportsWebP from 'supports-webp';
import { EventEmitterService } from './event-emitter.service';
import { Token, DefaultService } from '../api';
import { DevData } from '../interfaces/devData';
import { T24Url } from '../interfaces/t24Url';

const TOKEN = 'b2b_token';
const TOKEN_ALERT_BEFORE = 120; // minutes
const SESSION_LENGTH = 60; // minutes
const SESSION_END_ALERT_BEFORE = 60; // minutes

@Injectable()
export class AuthService implements OnDestroy, DevData, T24Url {
  public static getRequestParams(): Object {
    return {withCredentials: true};
  }

  public isLoggedInObs: Observable<boolean>;
  public tokenTimer: Observable<number>;
  public sessionTimer: Observable<number>;

  protected _userName: string;
  protected _token: string;
  protected _redirectURLAfterLogin: string;

  private _jwtHelper: JwtHelper = new JwtHelper();
  private _alive: boolean = true;
  private _loggingOut: boolean = false;
  private _tokenTimer: Subscription;
  private _sessionTimer: Subscription;
  private _isLoggedInSource = new BehaviorSubject<boolean>(false);
  private _tokenTimerSource = new BehaviorSubject<number>(null);
  private _sessionTimerSource = new BehaviorSubject<number>(null);

  constructor(private router: Router,
              private API: DefaultService,
              private eventEmitter: EventEmitterService) {
    this.isLoggedInObs = this._isLoggedInSource.asObservable();
    this.tokenTimer = this._tokenTimerSource.asObservable();
    this.sessionTimer = this._sessionTimerSource.asObservable();
    this.isLoggedIn = tokenNotExpired(TOKEN);
    this._token = localStorage.getItem(TOKEN);
  }

  set isLoggedIn(logged: boolean) {
    this._isLoggedInSource.next(logged);
  }

  get isLoggedIn(): boolean {
    return this._isLoggedInSource.getValue();
  }

  public getT24url(): string {
    return T24_URL;
  }

  public showDevData(): boolean {
    return SHOW_DEV_DATA;
  }

  public getHeaders(): Headers {
    return new Headers({
      'Webp-Supported': USE_WEBP && supportsWebP.toString(),
      'Image-Type': supportsWebP ? 'webp' : 'png'
    });
  }

  public afterLogin(header: Headers): void {
    if (this.checkTokenHeader(header)) {
      if (this._redirectURLAfterLogin && this._redirectURLAfterLogin.startsWith('http')) {
        window.location.href = this._redirectURLAfterLogin;
      } else {
        this.isLoggedIn = true;
        this._alive = true;
        this._setSessionTimer();
        this._setTokenTimer(this._token);
        this.router.navigate([this._redirectURLAfterLogin || '/configurator']);
      }
    } else {
      this.logout();
    }
  }

  public logout() {
    this._alive = false;
    if (!this._tokenTimer || !this._sessionTimer) {
      this._logout();
    }
  }

  public setRedirectURL(url: string) {
    this._redirectURLAfterLogin = url;
  }

  public setTokenTimer() {
    if (!this._tokenTimer) {
      this.setUserName(this._token);
      this._setTokenTimer(this._token);
    }
  }

  public resetSessionTimer() {
    this._setSessionTimer();
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  public checkTokenHeader(header: Headers): boolean {
    if (header.has('X-Token')) {
      this._token = header.get('X-Token');
      localStorage.setItem(TOKEN, this._token);
      this.setUserName(this._token);
      this._setTokenTimer(this._token);
      return tokenNotExpired(TOKEN);
    }
    return false;
  }

  public getUserName(): string {
    return this._userName;
  }

  protected setUserName(token: string) {
    if (token) {
      this._userName = this._jwtHelper.decodeToken(token).nam;
    }
  }

  private _logout() {
    if (!this._loggingOut) {
      this._loggingOut = true;
      this.API.logoutGet(AuthService.getRequestParams())
        .subscribe(
          data => {
            this._loggingOut = false;
          },
          error => {
            this._loggingOut = false;
            this.eventEmitter.error(error);
          }
        );
      this.isLoggedIn = false;
      localStorage.clear();
      sessionStorage.clear();
      this.router.navigate(['/login']);
    }
  }

  private _setSessionTimer() {
    let dueTime = (SESSION_LENGTH - SESSION_END_ALERT_BEFORE) * 60 * 1000;
    let cnt = SESSION_END_ALERT_BEFORE * 60 + 1;
    if (typeof this._sessionTimer !== 'undefined') {
      this._sessionTimer.unsubscribe();
    }
    this._sessionTimer = Observable.timer(dueTime, 1000)
      .takeWhile(() => this._alive)
      .take(cnt)
      .subscribe(
        t => {
          this._sessionTimerSource.next(cnt - 1 - t);
        },
        error => {
          console.log(error);
        },
        () => {
          this._sessionTimerSource.next(-1);
          this._logout();
        }
      );
  }

  private _setTokenTimer(token: string) {
    if (token && tokenNotExpired(TOKEN)) {
      let expDate = this._jwtHelper.getTokenExpirationDate(token);
      let now = new Date();
      let dueTime = expDate.getTime() - now.getTime() - (TOKEN_ALERT_BEFORE * 1000 * 60);
      let cnt = TOKEN_ALERT_BEFORE * 60 + 1 + (dueTime < 0 ? Math.floor(dueTime / 1000) : 0);
      if (typeof this._tokenTimer !== 'undefined') {
        this._tokenTimer.unsubscribe();
      }
      this._tokenTimer = Observable.timer(dueTime, 1000)
        .takeWhile(() => this._alive)
        .take(cnt)
        .subscribe(
          t => {
            this._tokenTimerSource.next(cnt - t);
          },
          error => {
            console.log(error);
          },
          () => {
            this._tokenTimerSource.next(-1);
            this._logout();
          }
        );
    } else {
      this._logout();
    }
  }

  // TODO only for dev
  private checkToken(token: Token): void {
    if (token === null) {
      token = {token: localStorage.getItem(TOKEN)};
    }
    console.log(
      this._jwtHelper.decodeToken(token.token),
      this._jwtHelper.getTokenExpirationDate(token.token),
      this._jwtHelper.isTokenExpired(token.token)
    );
  }
}
