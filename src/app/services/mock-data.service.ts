import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';

@Injectable()
export class MockDataService {
  constructor(private http: Http) {
  }

  public getData(position: string): Observable<any> {
    let file = 'mock-data.json';
    return this.http.get('./assets/mock-data/' + file)
      .map((res: any) => res.json()[position]);
  }
}
