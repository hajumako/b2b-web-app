import { Injectable, InjectionToken, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { EventEmitterService } from './event-emitter.service';
import { CacheService } from './cache.service';
import { DimensionsPipe } from '../pipes/dimensions.pipe';
import { ColorsCacheModel } from '../models/colors-cache.model';
import { ProductConfigurationModel } from '../models/product-configuration.model';
import { DefaultService, Image, RadiatorColors, ColorGroup, ProductConfiguration } from '../api';
import { RadiatorModel } from '../api/model/radiatorModel';
import { RadiatorSize } from '../api/model/radiatorSize';
import { RadiatorConnections } from '../api/model/radiatorConnections';
import { InventConfigurationStateTER } from '../api/model/inventConfigurationStateTER';
import { InfoModel } from '../models/info.model';
import { ProductGroupIdShort } from '../api/model/productGroupIdShort';
import { MockDataService } from './mock-data.service';

const CACHE_CONFIGURATION = new InjectionToken<string>('cache_product_configuration');
const CACHE_CONFIGURATION_HEATER = new InjectionToken<string>('cache_heater_configuration');
const CACHE_COLORS = new InjectionToken<string>('cache_color_groups');

@Injectable()
export class ConfiguratorService implements OnDestroy {
  protected _colorGroups: ColorGroup[] = null;
  protected _productConfiguration: ProductConfiguration = {
    color: null,
    configId: '',
    configurationState: null,
    connection: '',
    height: 0,
    image: null,
    images: null,
    itemId: '',
    label: '',
    model: '',
    name: '',
    price: 0,
    productGroup: '',
    sku: '',
    width: 0
  };

  protected _heaterConfiguration = {
    model: {
      label: '',
      itemId: '',
      power: null
    },
    colorId: '',
    cableId: '',
    configId: '',
  };

  private _configLoaded: boolean = false;
  private _alive: boolean = true;

  constructor(private API: DefaultService,
              private cacheService: CacheService,
              private authService: AuthService,
              private dimensionsPipe: DimensionsPipe,
              private eventEmitter: EventEmitterService,
              private mockDataService: MockDataService) {
    let cConfiguration = sessionStorage.getItem(CACHE_CONFIGURATION.toString());
    if (cConfiguration) {
      this._productConfiguration = JSON.parse(cConfiguration);
    }
  }

  public needsHeater(): boolean {
    return [
      ProductGroupIdShort.WL.toString(),
      ProductGroupIdShort.WS.toString(),
      ProductGroupIdShort.WW.toString()
    ]
      .indexOf(this.getType()) > -1;
  }

  public getProductGroup(): string {
    return this._productConfiguration.productGroup;
  }

  public setProductGroup(productGroup: string) {
    this._productConfiguration.productGroup = productGroup;
  }

  public getModel(): string {
    return this._productConfiguration.model;
  }

  public setModel(model: string) {
    this._productConfiguration.model = model;
  }

  public getLabel(): string {
    return this._productConfiguration.label;
  }

  public setLabel(label: string) {
    this._productConfiguration.label = label;
  }

  public getName(): string {
    return this._productConfiguration.name;
  }

  public setName(name: string) {
    this._productConfiguration.name = name;
    this._productConfiguration.height = parseInt(this.dimensionsPipe.transform(name, 'height', '/', ''), 10);
    this._productConfiguration.width = parseInt(this.dimensionsPipe.transform(name, 'width', '/', ''), 10);
  }

  public getHeight(): number {
    return this._productConfiguration.height;
  }

  public setHeight(height: number) {
    this._productConfiguration.height = height;
  }

  public getWidth(): number {
    return this._productConfiguration.width;
  }

  public setWidth(width: number) {
    this._productConfiguration.width = width;
  }

  public getItemId(): string {
    return this._productConfiguration.itemId;
  }

  public setItemId(itemId: string) {
    this._productConfiguration.itemId = itemId;
  }

  public getConfigId(): string {
    // TODO package (K) should be taken from API, not hardcoded
    this._productConfiguration.configId = 'K' + this._productConfiguration.color.colorId + this._productConfiguration.connection;
    return this._productConfiguration.configId;
  }

  public getConnection(): string {
    return this._productConfiguration.connection;
  }

  public setConnection(connection: string) {
    this._productConfiguration.connection = connection;
  }

  public getColor(): RadiatorColors {
    return this._productConfiguration.color;
  }

  public setColor(color: RadiatorColors) {
    this._productConfiguration.color = color;
  }

  public getImage(): Image {
    return this._productConfiguration.image;
  }

  public setImage(image: Image) {
    this._productConfiguration.image = image;
  }

  public getImages(): Image[] {
    return this._productConfiguration.images;
  }

  public setImages(images: Image[]) {
    this._productConfiguration.images = images;
  }

  public getPrice(): number {
    // TODO connect to WS when ready
    return this._productConfiguration.price;
  }

  public setPrice(price: number) {
    this._productConfiguration.price = price;
  }

  public getSKU(): string {
    this._productConfiguration.sku = this.getItemId() + '-' + this.getConfigId();
    return this._productConfiguration.sku;
  }

  public getType(): string {
    return this.getModel().substring(0, 2);
  }

  public getConfiguration(): ProductConfiguration {
    return this._productConfiguration;
  }

  public saveConfiguration() {
    sessionStorage.setItem(CACHE_CONFIGURATION.toString(), JSON.stringify(this._productConfiguration));
  }

  public getHeaterModel(): any {
    return this._heaterConfiguration.model;
  }

  public setHeaterModel(model: any) {
    this._heaterConfiguration.model = model;
  }

  /*public getHeaterLabel(): string {
    return this._heaterConfiguration.label;
  }

  public setHeaterLabel(label: string) {
    this._heaterConfiguration.label = label;
  }

  public getHeaterItemId(): string {
    return this._heaterConfiguration.itemId;
  }

  public setHeaterItemId(itemId: string) {
    this._heaterConfiguration.itemId = itemId;
  }
*/
  public getHeaterColorId(): string {
    return this._heaterConfiguration.colorId;
  }

  public setHeaterColorId(colorId: string) {
    this._heaterConfiguration.colorId = colorId;
    this.checkAndSetHeaterConfigId();
  }

  public getHeaterCableId(): string {
    return this._heaterConfiguration.cableId;
  }

  public setHeaterCableId(cableId: string) {
    this._heaterConfiguration.cableId = cableId;
    this.checkAndSetHeaterConfigId();
  }

  public getHeaterConfigId(): string {
    return this._heaterConfiguration.configId;
  }

  public setHeaterConfigId(configId: string) {
    this._heaterConfiguration.configId = configId;
  }

  public saveHeaterConfiguration() {
    sessionStorage.setItem(CACHE_CONFIGURATION_HEATER.toString(), JSON.stringify(this._heaterConfiguration));
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  public getColorGroups() {
    if (this._colorGroups) {
      return this._colorGroups;
    } else {
      let colorsCache = <ColorsCacheModel> this.cacheService.getCache(CACHE_COLORS.toString());
      if (colorsCache !== null) {
        if (colorsCache.expires - new Date().getTime() > 0) {
          this._colorGroups = colorsCache.colorGroups;
          return this._colorGroups;
        }
      }
      this.APIgetColorGroups();
    }
  }

  public checkModel(model: string) {
    this._configLoaded = false;
    if (model !== this.getModel()) {
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorCheckModelModelGetWithHttpInfo(model, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('configuratorModelGroupIdModel', data);
            let radiatorModels = <RadiatorModel[]> data.json();
            if (radiatorModels.length) {
              this.setColor(null);
              this.setImage(radiatorModels[0].image);
              this.setImages(null);
              this.setItemId('');
              this.setLabel(radiatorModels[0].labelName);
              this.setModel(radiatorModels[0].radiatorModel);
              this.setName('');
              this.saveConfiguration();
              this._configLoaded = true;
            } else {
              this.eventEmitter.info({
                title: 'Błąd',
                msg: 'Model ' + model + ' nie istnieje',
                redirect: '/configurator'
              });
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    } else {
      this._configLoaded = true;
    }
  }

  public checkItemId(itemId: string) {
    this._configLoaded = false;
    if (itemId !== this.getItemId()) {
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorCheckSizeItemIdGetWithHttpInfo(itemId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('configuratorSizeItemId', data);
            let radiatorSizes = <RadiatorSize[]> data.json();
            if (radiatorSizes.length) {
              this.setItemId(radiatorSizes[0].itemId);
              this.setName(radiatorSizes[0].name);
              this.setImage(radiatorSizes[0].image);
              this.setImages(radiatorSizes[0].images);
              this.saveConfiguration();
              this._configLoaded = true;
            } else {
              this.eventEmitter.info({
                title: 'Błąd',
                msg: 'Kod pozycji ' + itemId + ' nie istnieje',
                redirect: '/configurator'
              });
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    } else {
      this._configLoaded = true;
    }
  }

  public checkConnection(itemId: string, connectionId: string) {
    this._configLoaded = false;
    this.checkItemId(itemId);
    if (itemId !== this.getItemId() || connectionId !== this.getConnection()) {
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorCheckConnectionItemIdConnectionIdGetWithHttpInfo(itemId, connectionId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('configuratorCheckConnectionItemIdConnectionId', data);
            let radiatorConnections = <RadiatorConnections[]> data.json();
            if (radiatorConnections.length) {
              this.setConnection(radiatorConnections[0].connectionId);
              this.saveConfiguration();
              this._configLoaded = true;
            } else {
              this.eventEmitter.info({
                title: 'Błąd',
                msg: 'Podłączenie ' + connectionId + ' dla ' + itemId + ' nie jest możliwe',
                redirect: '/configurator'
              });
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    } else {
      this._configLoaded = true;
    }
  }

  public checkColor(itemId: string, colorId: string) {
    this._configLoaded = false;
    this.checkItemId(itemId);
    if (itemId !== this.getItemId() || colorId !== this.getColor().colorId) {
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorCheckColorItemIdColorIdGetWithHttpInfo(itemId, colorId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('configuratorCheckColorItemIdColorId', data);
            let radiatorColors = <RadiatorColors[]> data.json();
            if (radiatorColors.length) {
              this.setColor(radiatorColors[0]);
              this.saveConfiguration();
            } else {
              this.eventEmitter.info({
                title: 'Błąd',
                msg: 'Kolor ' + colorId + ' dla ' + itemId + ' nie jest możliwy',
                redirect: '/configurator'
              });
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );

    } else {
      this._configLoaded = true;
    }
  }

  public checkConfig(itemId: string, connectionId: string, colorId: string) {
    this._configLoaded = false;
    if (itemId !== this.getItemId() || connectionId !== this.getConnection() || colorId !== this.getColor().colorId) {
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorConfigGetItemIdConfigIdGetWithHttpInfo(itemId, 'K' + colorId + connectionId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('configuratorConfigGetItemIdConfigId', data.json());
            let productConfiguration = <ProductConfiguration> data.json();
            if ([InventConfigurationStateTER.Possible, InventConfigurationStateTER.Released].indexOf(productConfiguration.configurationState) > -1) {
              this._productConfiguration = data.json();
              this.saveConfiguration();
              this._configLoaded = true;
            } else {
              this.eventEmitter.info({
                title: 'Błąd',
                msg: 'Konfiguracja ' + itemId + '-' + 'K' + colorId + connectionId + ' nie jest możliwa',
                redirect: '/configurator'
              });
            }
          }
        );
    } else {
      this._configLoaded = true;
    }
  }

  public checkHeaterModel(itemId: string, connectionId: string, colorId: string, heaterItemId: string) {
    this._configLoaded = false;
    this.checkConfig(itemId, connectionId, colorId);
    if (heaterItemId !== this.getHeaterModel().itemId) {
      // TODO dummy until WS is ready
      this.mockDataService.getData('heaterData')
        .subscribe(
          data => {
            //if (data.hasOwnProperty(heaterItemId)) {
              this.setHeaterModel(data['WEMOA01']);
              this.saveHeaterConfiguration();
              this._configLoaded = true;
            /*} else {
              this.eventEmitter.info({
                title: 'Błąd',
                msg: 'Grzałka ' + heaterItemId + ' nie istnieje',
                redirect: '/configurator'
              });
            }*/
          }
        );
    } else {
      this._configLoaded = true;
    }
  }

  public checkHeaterConfig(itemId: string, connectionId: string, colorId: string, heaterItemId: string, heaterConfigId: string) {
    this._configLoaded = false;
    this.checkConfig(itemId, connectionId, colorId);
    let _colorId = heaterConfigId.substring(1, 4);
    let _cableId = heaterConfigId.substring(4, 5);
    let ok = [0, 0];
    console.log('cc', _colorId, _cableId);
    if (heaterItemId !== this.getHeaterModel().itemId) {
      ok[0] = 1;
      this.mockDataService.getData('heaterColors')
        .subscribe(
          data => {
            if (data.hasOwnProperty(heaterItemId)) {
              for (let color of data[heaterItemId]) {
                if (color.colorId === _colorId) {
                  ok[0] = 2;
                  return;
                }
              }
              if (ok[0] === 1) {
                ok[0] = 3;
              }
              if (ok[1] === 2) {
                //this._configLoaded = true;
              }
            }
          }
        );
    }
    if (heaterConfigId !== this.getHeaterConfigId()) {
      ok[1] = 1;
      this.mockDataService.getData('heaterCables')
        .subscribe(
          data => {
            if (data.hasOwnProperty(heaterItemId)) {
              for (let cable of data[heaterItemId]) {
                if (cable.cableId === _cableId) {
                  ok[1] = 2;
                  return;
                }
              }
              if (ok[1] === 1) {
                ok[1] = 3;
              }
              if (ok[0] === 2) {
                //this._configLoaded = true;
              }
            }
          }
        );
    }
  }

  public isConfigLoaded(): boolean {
    return this._configLoaded;
  }

  protected checkAndSetHeaterConfigId() {
    if (this._heaterConfiguration.colorId !== '' && this._heaterConfiguration.cableId !== '') {
      this._heaterConfiguration.configId = 'T' + this._heaterConfiguration.colorId + this._heaterConfiguration.cableId;
    }
  }

  protected APIgetColorGroups() {
    this.API.colorsIngroupsGetWithHttpInfo(AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('colorsIngroups', data);
          this._colorGroups = <ColorGroup[]> data.json();
          let colors: ColorsCacheModel = {
            expires: new Date().getTime() + (CACHE_EXP * 24 * 60 * 60 * 1000),
            colorGroups: this._colorGroups
          };
          this.cacheService.saveCache(CACHE_COLORS.toString(), colors);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }
}
