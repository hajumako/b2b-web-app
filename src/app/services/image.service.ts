import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ImageService {
  public url: string;

  protected cdnUrl: string = 'http://' + process.env.CDN_URL + '/';
  protected cam: string = 'CAM1';

  constructor() {

  }

  public getImg() {
    return Observable.of(this.url);
  }

  public getImage(name: string,
                  size: number[] = [540, 300],
                  color: string = 'RAL 1015',
                  connection: string = 'SX'
  ): Observable<string> {
    let url = this.cdnUrl + name + '/' + this.cam + '/' + name.toUpperCase() + '_' + size[0] + '_' + size[1] + '_' + color.replace(' ', '_') + '_' + connection + '.png';
    console.log(url);
    return Observable.of(url);
  }
}
