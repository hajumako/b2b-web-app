import { Injectable, InjectionToken, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeWhile';
import { AuthService } from './auth.service';
import { EventEmitterService } from './event-emitter.service';
import { ParserModel } from '../models/parser.model';
import {
  ResponseCode,
  DefaultService,
  InventConfigurationStateStructTER,
  PriceCustomerResponseTER
} from '../api';

const PARSER = new InjectionToken<string>('parser');

@Injectable()
export class ParserService implements OnDestroy {

  protected _parser: ParserModel = {
    codesString: '',
    codesList: <string[]> [],
    validationResults: <InventConfigurationStateStructTER[]> []
  };

  private _alive: boolean = true;

  constructor(private API: DefaultService,
              private authService: AuthService,
              private eventEmitter: EventEmitterService) {
    let p = sessionStorage.getItem(PARSER.toString());
    if (p) {
      this._parser = <ParserModel> JSON.parse(p);
    }
  }

  public getCodesString(): string {
    return this._parser.codesString;
  }

  public setCodesList(codesList: string): Observable<InventConfigurationStateStructTER[]> {
    this._parser.codesString = codesList.replace(/([;|\s|,])+/gm, '\n');
    this._parser.codesList = this._parser.codesString.split('\n');

    return this.APIvalidateCodes(this._parser.codesList, true);
  }

  public getCodesList(): string[] {
    return this._parser.codesList;
  }

  public validateCodesList(codesList: string): Observable<InventConfigurationStateStructTER[]> {
    return this.APIvalidateCodes(codesList.replace(/([;|\s|,])+/gm, '\n').split('\n'), false);
  }

  public getValidationResult(): InventConfigurationStateStructTER[] {
    return this._parser.validationResults;
  }

  public getPrices(skus: string[]) {
    return this.APIgetPrices(skus);
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  private APIvalidateCodes(codesList: string[], save: boolean): Observable<InventConfigurationStateStructTER[]> {
    return this.API.parserCodesValidatePostWithHttpInfo(codesList, AuthService.getRequestParams())
      .map(
        (response: Response) => {
          console.log('parserCodesValidate', response);
          this.authService.checkTokenHeader(response.headers);
          if (response.status === ResponseCode.NUMBER_200) {
            let validationResults = <InventConfigurationStateStructTER[]> response.json();
            if (save) {
              this._parser.validationResults = validationResults;
              sessionStorage.setItem(PARSER.toString(), JSON.stringify(this._parser));
            }
            return validationResults;
          }
        }
      );
  }

  private APIgetPrices(skus: string[]): Observable<Object> {
    return this.API.configuratorPricesGetPostWithHttpInfo(skus, AuthService.getRequestParams())
      .map(
        (response: Response) => {
          console.log('configuratorPricesGet', response);
          this.authService.checkTokenHeader(response.headers);
          if (response.status === ResponseCode.NUMBER_200) {
            let prices = [];
            (<PriceCustomerResponseTER[]> response.json()).forEach(priceData => {
              prices[priceData.itemId + '-' + priceData.configId] = priceData;
            });
            return prices;
          }
        }
      );
  }
}
