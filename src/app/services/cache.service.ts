import { Injectable, InjectionToken } from '@angular/core';

const CACHE = new InjectionToken<string>('cache');

@Injectable()
export class CacheService {

  constructor() {
    // TODO fill up
  }

  public saveCache(key: string, data: any) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  public getCache(key: string): any {
    return JSON.parse(localStorage.getItem(key));
  }

  public removeCache(key: string) {
    localStorage.removeItem(key);
  }

  public clearAllCache() {
    localStorage.clear();
  }
}
