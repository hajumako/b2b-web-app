import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';

declare let $: any;

@Component({
  styleUrls: ['./cart.component.scss'],
  templateUrl: './cart.component.html'
})
export class CartComponent implements OnInit {
  public title = 'Koszyk';
  public configs: Object;

  public loading: boolean[] = [];
  public modalId: string = 'cartModal';

  constructor(public cartService: CartService) {
    this.cartService.getConfigs(this.cartService.getCartSkus())
      .subscribe(
        data => {
          this.configs = data;
        }
      );
  }

  public isLoading(i: number) {
    if (typeof this.loading[i] === 'undefined') {
      this.loading[i] = false;
    }
    return this.loading[i];
  }

  public openModal() {
    $('#' + this.modalId).foundation('open');
  }

  public ngOnInit() {
    $(document).foundation();
  }
}
