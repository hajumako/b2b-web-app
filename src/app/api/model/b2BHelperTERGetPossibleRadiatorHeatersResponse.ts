/**
 * Terma B2B API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.1
 * Contact: konrad.kucharski@termagroup.pl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { ItemIdConfigIdContractTER } from './itemIdConfigIdContractTER';


export interface B2BHelperTERGetPossibleRadiatorHeatersResponse {
    /**
     * $response
     */
    response?: Array<ItemIdConfigIdContractTER>;

}
