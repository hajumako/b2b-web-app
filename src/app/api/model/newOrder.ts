/**
 * Terma B2B API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.1
 * Contact: konrad.kucharski@termagroup.pl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { Address } from './address';
import { DeliveryTypeInterface } from './deliveryTypeInterface';
import { LineItem } from './lineItem';
import { OrderStatusInterface } from './orderStatusInterface';
import { PaymentMethodInterface } from './paymentMethodInterface';


export interface NewOrder {
    /**
     * 
     */
    uid: number;

    /**
     * 
     */
    status?: OrderStatusInterface;

    /**
     * 
     */
    lineItems?: Array<LineItem>;

    /**
     * 
     */
    addresses?: Array<Address>;

    /**
     * 
     */
    deliveryType?: DeliveryTypeInterface;

    /**
     * 
     */
    paymentMethod?: PaymentMethodInterface;

}
