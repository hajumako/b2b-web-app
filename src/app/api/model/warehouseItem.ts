/**
 * Terma B2B API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.1
 * Contact: konrad.kucharski@termagroup.pl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { Image } from './image';
import { ProductConfiguration } from './productConfiguration';


export interface WarehouseItem {
    /**
     * 
     */
    nid?: number;

    /**
     * 
     */
    modelName?: string;

    /**
     * 
     */
    productId?: number;

    /**
     * 
     */
    sku?: string;

    /**
     * 
     */
    configurationName?: string;

    /**
     * 
     */
    quantity?: number;

    /**
     * 
     */
    price?: number;

    /**
     * 
     */
    priceRrp?: number;

    /**
     * 
     */
    sale?: boolean;

    /**
     * 
     */
    image?: Image;

    /**
     * 
     */
    config?: ProductConfiguration;

}
