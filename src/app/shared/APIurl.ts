import { Injectable } from '@angular/core';
import '../api/rxjs-operators';
import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';
import { DefaultService } from '../api/api/default.service';

@Injectable()
export class APIurl extends DefaultService {
  public get() {
    console.log('API url:', this.basePath);
    return this.basePath;
  }
}
