import { Component, Input } from '@angular/core';
import { InfoModel } from '../../models/info.model';

@Component({
  selector: 'b2b-info-modal',
  styleUrls: ['./info-modal.component.scss'],
  templateUrl: 'info-modal.component.html'
})
export class InfoModalComponent {
  @Input() public modalId: string;
  @Input() public info: InfoModel;
}
