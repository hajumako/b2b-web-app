import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService) {
  }

  public canActivate(route: ActivatedRouteSnapshot,
                     state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.authService.isLoggedIn) {
      this.authService.resetSessionTimer();
      this.authService.setTokenTimer();
      return true;
    } else {
      this.authService.setRedirectURL(state.url);
      this.authService.logout();
      return false;
    }
  }
}
