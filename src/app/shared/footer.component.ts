import { Component, Input } from '@angular/core';

@Component({
  selector: 'b2b-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html'
})
export class FooterComponent {
  @Input() public isLoggedIn: boolean;

  constructor() {
    // TODO
  }
}
