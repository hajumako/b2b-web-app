import { Component, Input } from '@angular/core';
import { CartService } from '../services/cart.service';
import { APIurl } from './APIurl';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'b2b-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  @Input() public isLoggedIn: boolean;

  constructor(public cartService: CartService,
              private authService: AuthService,
              private APIurl: APIurl) {
    APIurl.get();
  }

  public onSearch(event) {
    console.log('search', event);
  }

  public onLogout() {
    this.authService.logout();
  }
}
