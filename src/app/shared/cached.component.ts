import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { EventEmitterService } from '../services/event-emitter.service';
import { CacheService } from '../services/cache.service';

@Component({
  template: ''
})
export class CachedComponent implements OnDestroy {
  protected alive: boolean = true;
  protected backNav: boolean = false;
  protected eventEmitterSubscr: Subscription;

  constructor(protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    this.eventEmitterSubscr = this.eventEmitter.event
      .takeWhile(() => this.alive)
      .filter(code => code === 'backNav')
      .subscribe(
        event => {
          this.backNav = true;
          console.log('backNav');
        });
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
