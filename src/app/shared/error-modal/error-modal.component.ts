import { Component, Input } from '@angular/core';
import { ErrorModel } from '../../api/model/errorModel';
import { DevData } from '../../interfaces/devData';

@Component({
  selector: 'b2b-error-modal',
  styleUrls: ['./error-modal.component.scss'],
  templateUrl: 'error-modal.component.html'
})
export class ErrorModalComponent implements DevData {
  public readonly messages = {
    0: 'Nie można nawiązać połączenia z API',
    400: 'Błąd ogólny',
    401: 'Nieuatoryzowany dostęp',
    403: 'Brak dostępu',
    405: 'Nierozpoznane zapytanie',
    500: 'Błąd bazy danych',
    501: 'Błąd wewnętrzny API',
    502: 'Błąd połączenia z webservice'
  };

  @Input() public modalId: string;
  public status: number = 0;
  public errorBody: ErrorModel;

  private _error;

  @Input()
  set error(value: any) {
    this._error = value;
    try {
      if (value && value.hasOwnProperty('_body')) {
        this.status = value.status;
        this.errorBody = <ErrorModel> JSON.parse(value._body);
      }
    } catch (e) {
      console.log('cannot parse error', e.message);
    }
  }

  get error(): any {
    return this._error;
  }

  public showDevData() {
    return SHOW_DEV_DATA;
  }
}
