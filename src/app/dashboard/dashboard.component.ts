import { AfterViewInit, Component, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '../app.service';
import { AuthService } from '../services/auth.service';
import { CacheService } from '../services/cache.service';
import { EventEmitterService } from '../services/event-emitter.service';
import { CachedComponent } from '../shared/cached.component';
import { DefaultService, Heater, Radiator, Accessory, ResponseCode, ProductGroupId, RadiatorModel } from '../api';
import { Image } from '../api/model/image';

const CACHE_DAS_A = new InjectionToken<string>('das_a');
const CACHE_DAS_H = new InjectionToken<string>('das_h');
const CACHE_DAS_R = new InjectionToken<string>('das_r');

const CACHE_DAS_A_T = new InjectionToken<string>('das_a_t');
const CACHE_DAS_H_T = new InjectionToken<string>('das_h_t');
const CACHE_DAS_R_T = new InjectionToken<string>('das_r_t');

const CACHE_DAS_A_C = new InjectionToken<string>('das_a_c');
const CACHE_DAS_H_C = new InjectionToken<string>('das_h_c');
const CACHE_DAS_R_C = new InjectionToken<string>('das_r_c');

const CACHE_DAS_A_I = new InjectionToken<string>('das_a_i');
const CACHE_DAS_H_I = new InjectionToken<string>('das_h_i');
const CACHE_DAS_R_I = new InjectionToken<string>('das_r_i');

const CACHE_ACTIVE_TAB = new InjectionToken<string>('cache_dashboard_active_tab');

@Component({
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent extends CachedComponent implements OnInit, AfterViewInit {
  public title = 'Dashboard';
  public activeTab: number = 0;

  public accessories: Accessory[];
  public heaters: Heater[];
  public radiators: Radiator[];
  public imagesAcc: Image[];
  public imagesHea: Image[];
  public imagesRad: Image[];
  public totalAcc: number = 0;
  public totalHea: number = 0;
  public totalRad: number = 0;
  public currentPageAcc: number = 1;
  public currentPageHea: number = 1;
  public currentPageRad: number = 1;
  public limit: number;

  public productGroups = [
    'Grzejniki',
    'Grzałki',
    'Akcesoria'
  ];

  private _lang: string;
  private _fragment: string = null;

  constructor(private authService: AuthService,
              private API: DefaultService,
              private appState: AppState,
              private router: Router,
              private route: ActivatedRoute,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    this._lang = appState.get('lang');
    this.limit = appState.get('limit');

    if (this.backNav) {
      this.accessories = <Accessory[]> cacheService.getCache(CACHE_DAS_A.toString());
      this.heaters = <Heater[]> cacheService.getCache(CACHE_DAS_H.toString());
      this.radiators = <Radiator[]> cacheService.getCache(CACHE_DAS_R.toString());
      this.totalAcc = this.cacheService.getCache(CACHE_DAS_A_T.toString());
      this.totalHea = this.cacheService.getCache(CACHE_DAS_H_T.toString());
      this.totalRad = this.cacheService.getCache(CACHE_DAS_R_T.toString());
      this.currentPageAcc = this.cacheService.getCache(CACHE_DAS_A_C.toString());
      this.currentPageHea = this.cacheService.getCache(CACHE_DAS_H_C.toString());
      this.currentPageRad = this.cacheService.getCache(CACHE_DAS_R_C.toString());
      this.imagesAcc = this.cacheService.getCache(CACHE_DAS_A_I.toString());
      this.imagesHea = this.cacheService.getCache(CACHE_DAS_R_I.toString());
      this.imagesRad = this.cacheService.getCache(CACHE_DAS_R_I.toString());
      this.activeTab = <number> this.cacheService.getCache(CACHE_ACTIVE_TAB.toString());
    } else {
      this.activateTab(this.activeTab);
    }
  }

  public ngOnInit(): any {
    this.route.fragment.subscribe(fragment => {
      this._fragment = fragment;
    });
  }

  public ngAfterViewInit(): void {
    try {
      switch (this._fragment) {
        case 'radiators':
          this.activateTab(0);
          break;
        case 'heaters':
          this.activateTab(1);
          break;
        case 'accessories':
          this.activateTab(2);
          break;
        default:
          this.activateTab(0);
      }
    } catch (e) {
      console.log('DashboardComponent error', e);
    }
  }

  public activateTab(i: number) {
    this.activeTab = i;
    this.cacheService.saveCache(CACHE_ACTIVE_TAB.toString(), this.activeTab);
    if (i === 0 && !this.radiators) {
      this.getRadiatorsPage(1);
    } else if (i === 1 && !this.heaters) {
      this.getHeatersPage(1);
    } else if (i === 2 && !this.accessories) {
      this.getAccessoriesPage(1);
    }
  }

  public getAccessoriesPage(page: number) {
    this.accessories = null;
    this.API.accessoriesLangListGetWithHttpInfo(this._lang, null, null, this.limit, this.limit * (page - 1), AuthService.getRequestParams())
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            if (data.headers.has('X-Total')) {
              this.totalAcc = Number(data.headers.get('X-Total'));
              this.cacheService.saveCache(CACHE_DAS_A_T.toString(), this.totalAcc);
            }
            this.accessories = <Accessory[]> data.json();
            this.currentPageAcc = page;
            this.cacheService.saveCache(CACHE_DAS_A_C.toString(), page);
            this.cacheService.saveCache(CACHE_DAS_A.toString(), this.accessories);
            this.getAccessoriesImages();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public getHeatersPage(page: number) {
    this.heaters = null;
    this.API.heatersLangListGetWithHttpInfo(this._lang, null, null, this.limit, this.limit * (page - 1), AuthService.getRequestParams())
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            if (data.headers.has('X-Total')) {
              this.totalHea = Number(data.headers.get('X-Total'));
              this.cacheService.saveCache(CACHE_DAS_H_T.toString(), this.totalHea);
            }
            this.heaters = <Heater[]> data.json();
            this.currentPageHea = page;
            this.cacheService.saveCache(CACHE_DAS_H_C.toString(), page);
            this.cacheService.saveCache(CACHE_DAS_H.toString(), this.heaters);
            this.getHeatersImages();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public getRadiatorsPage(page: number) {
    this.radiators = null;
    this.API.radiatorsLangListGetWithHttpInfo(this._lang, null, null, this.limit, this.limit * (page - 1), AuthService.getRequestParams())
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            if (data.headers.has('X-Total')) {
              this.totalRad = Number(data.headers.get('X-Total'));
              this.cacheService.saveCache(CACHE_DAS_R_T.toString(), this.totalRad);
            }
            this.radiators = <Radiator[]> data.json();
            this.currentPageRad = page;
            this.cacheService.saveCache(CACHE_DAS_R_C.toString(), page);
            this.cacheService.saveCache(CACHE_DAS_R.toString(), this.radiators);
            this.getRadiatorsImages();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public accessoryDetails(index: number) {
    this.router.navigate(['/accessories/item', this.accessories[index].nid]);
  }

  public heaterDetails(index: number) {
    this.router.navigate(['/heaters/item', this.heaters[index].nid]);
  }

  public radiatorDetails(index: number) {
    this.router.navigate(['/radiators/item', this.radiators[index].nid]);
  }

  private getAccessoriesImages() {
    this.imagesAcc = null;
    let names = [];
    this.accessories.forEach(radiator => {
      names.push(radiator.modelName);
    });
    this.API.imagesBylabelGetPostWithHttpInfo(names, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('imagesBylabelGet', data);
          this.imagesAcc = <Image[]> data.json();
          this.cacheService.saveCache(CACHE_DAS_A_I.toString(), this.imagesAcc);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private getHeatersImages() {
    this.imagesHea = null;
    let names = [];
    this.heaters.forEach(radiator => {
      names.push(radiator.modelName);
    });
    this.API.imagesBylabelGetPostWithHttpInfo(names, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('imagesBylabelGet', data);
          this.imagesHea = <Image[]> data.json();
          this.cacheService.saveCache(CACHE_DAS_H_I.toString(), this.imagesHea);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private getRadiatorsImages() {
    this.imagesRad = null;
    let names = [];
    this.radiators.forEach(radiator => {
      names.push(radiator.modelName);
    });
    this.API.imagesBylabelGetPostWithHttpInfo(names, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('imagesBylabelGet', data);
          this.imagesRad = <Image[]> data.json();
          this.cacheService.saveCache(CACHE_DAS_R_I.toString(), this.imagesRad);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }
}
