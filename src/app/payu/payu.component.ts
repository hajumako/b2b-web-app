import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  styleUrls: ['./payu.component.scss'],
  templateUrl: './payu.component.html'
})
export class PayuComponent implements OnDestroy {
  public title = 'Payu';

  protected alive: boolean = true;

  constructor() {
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
