import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './payu.routes';
import { PipesModule } from '../pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { StartComponent } from './start/start.component';
import { PayuComponent } from './payu.component';

@NgModule({
  declarations: [
    PayuComponent,
    StartComponent
  ],
  exports: [],
  imports: [
    PipesModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
})
export class PayuModule {
  public static routes = routes;
}
