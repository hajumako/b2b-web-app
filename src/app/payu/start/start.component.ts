import { Component } from '@angular/core';
import { DefaultService } from '../../api/api/default.service';
import { Payment } from '../../api/model/payment';
import { CartService } from '../../services/cart.service';

@Component({
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent {
  public payment: Payment;

  constructor(private cartService: CartService,
              private API: DefaultService) {
   this.payment = cartService.getPayment();
  }
}
