import { AuthGuard } from '../shared/auth.guard';
import { StartComponent } from './start/start.component';
import { PayuComponent } from './payu.component';

export const routes = [
  {
    path: '', component: PayuComponent, children: [
    {path: '', pathMatch: 'full', redirectTo: 'start'},
    {path: 'start', component: StartComponent, canActivate: [AuthGuard]},
  ]
  },
];
