import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dev-box',
  styleUrls: ['./dev-box.component.scss'],
  templateUrl: './dev-box.component.html'
})
export class DevBoxComponent implements OnInit{
  @Input() public label: string;
  @Input() public title: string;
  @Input() public cssClasses: string;
  @Input() public noTitle: boolean = false;


  public ngOnInit() {
    if (!this.title && !this.noTitle) {
      switch (this.label.toLowerCase()) {
        case 'dr':
          this.title = 'Drupal data:';
          break;
        case 'ax':
          this.title = 'AX data:';
          break;
        default:
          this.title = 'Data:';
      }
    }
  }
}
