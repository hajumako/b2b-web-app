import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DevBoxComponent } from './dev-box/dev-box.component';
import { routes } from './_dev-components.routes';
import { ConnectionsComponent } from './connections/connections.component';

@NgModule({
  declarations: [
    DevBoxComponent,
    ConnectionsComponent
  ],
  exports: [
    DevBoxComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class _DevComponentsModule {
  public static routes = routes;
}
