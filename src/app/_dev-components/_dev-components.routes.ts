import { ConnectionsComponent } from './connections/connections.component';

export const routes = [
  {path: 'connections', component: ConnectionsComponent}
];
