export interface StepModel {
  type: string;
  step: number;
}
