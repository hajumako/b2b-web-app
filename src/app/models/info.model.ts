export interface InfoModel {
  title: string;
  msg: string;
  redirect: string;
}
