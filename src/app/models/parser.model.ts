import { Image } from '../api/model/image';
import { InventConfigurationStateStructTER } from '../api/model/inventConfigurationStateStructTER';

export interface ParserModel {
  codesString: string;
  codesList: string[];
  validationResults: InventConfigurationStateStructTER[];
}

export interface CodeModel {
  code: string;
  image: Image;
}
