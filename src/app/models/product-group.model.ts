import { GroupedModels } from '../api/model/groupedModels';

export interface ProductGroupModel {
  id: string;
  name: string;
  models: GroupedModels[];
}
