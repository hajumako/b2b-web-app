import { LogisticsAddressCountryRegionContract } from '../api/model/logisticsAddressCountryRegionContract';

export interface CountriesCacheModel {
  expires: number;
  countries: LogisticsAddressCountryRegionContract[];
}
