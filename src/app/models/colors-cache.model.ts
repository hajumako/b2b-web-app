import { ColorGroup } from '../api/model/colorGroup';

export interface ColorsCacheModel {
  expires: number;
  colorGroups: ColorGroup[];
}
