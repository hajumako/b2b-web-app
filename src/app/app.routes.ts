import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/auth.guard';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WarehouseComponent } from './warehouse/warehouse/warehouse.component';
import { WarehouseDetailsComponent } from './warehouse/details/warehouseDetails.component';
import { CartComponent } from './cart/cart.component';
import { HeaterComponent } from './details/heater.component';
import { RadiatorComponent } from './details/radiator.component';
import { AccessoryComponent } from './details/accessory.component';
import { NoContentComponent } from './no-content/no-content.component';
import { AXWarehouseComponent } from './warehouse/ax-warehouse/ax-warehouse.component';

const ROUTES: Routes = [
  {path: 'dev', loadChildren: './_dev-components#_DevComponentsModule'},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'radiators/item/:itemId', component: RadiatorComponent, canActivate: [AuthGuard]},
  {path: 'heaters/item/:itemId', component: HeaterComponent, canActivate: [AuthGuard]},
  {path: 'accessories/item/:itemId', component: AccessoryComponent, canActivate: [AuthGuard]},
  {path: 'warehouse', component: WarehouseComponent, canActivate: [AuthGuard]},
  {path: 'ax-warehouse', component: AXWarehouseComponent, canActivate: [AuthGuard]},
  {path: 'warehouse/item/:productId', component: WarehouseDetailsComponent, canActivate: [AuthGuard]},
  {path: 'ax-warehouse/item/:itemId/:configId', component: WarehouseDetailsComponent, canActivate: [AuthGuard]},
  {path: 'cart', component: CartComponent, canActivate: [AuthGuard]},
  {path: 'parser', loadChildren: './parser#ParserModule', canActivate: [AuthGuard]},
  {path: 'configurator', loadChildren: './configurator#ConfiguratorModule', canActivate: [AuthGuard]},
  {path: 'account', loadChildren: './account#AccountModule', canActivate: [AuthGuard]},
  {path: 'order', loadChildren: './order#OrderModule', canActivate: [AuthGuard]},
  {path: 'payu', loadChildren: './payu#PayuModule', canActivate: [AuthGuard]},
  {path: '**', component: NoContentComponent},
];

export const routing = RouterModule.forRoot(ROUTES, {useHash: false, preloadingStrategy: PreloadAllModules});
