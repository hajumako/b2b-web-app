import { Component, OnDestroy } from '@angular/core';
import { EventEmitterService } from '../services/event-emitter.service';
import { ConfiguratorService } from '../services/configurator.service';
import { StepModel } from '../models/step.model';

@Component({
  styleUrls: ['./configurator.component.scss'],
  templateUrl: './configurator.component.html'
})
export class ConfiguratorComponent implements OnDestroy {
  public title = 'Konfiguracja grzejnika';
  public currentStep: number = -1;

  protected alive: boolean = true;

  constructor(public configuratorService: ConfiguratorService,
              private eventEmitter: EventEmitterService) {
    eventEmitter.event
      .takeWhile(() => this.alive)
      .filter(code => EventEmitterService.isOfType(code, eventEmitter.CONFIGURATOR))
      .subscribe(
        event => {
          this.currentStep = (<StepModel> JSON.parse(event)).step;
        }
      );
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
