import { Component, OnDestroy } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { AuthService } from '../../services/auth.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { StepModel } from '../../models/step.model';
import { DefaultService, PriceCustomerResponseTER, ResponseCode } from '../../api';
import { ActivatedRoute } from '@angular/router';

const STEP: number = 5;

@Component({
  styleUrls: ['./summary.component.scss'],
  templateUrl: './summary.component.html'
})
export class SummaryComponent implements OnDestroy {
  public quantity: number = 1;
  public nextStep: boolean = false;
  public price: number = -1;

  protected alive: boolean = true;

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private cartService: CartService,
              private eventEmitter: EventEmitterService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute) {
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.configuratorService.needsHeater()) {
      this.configuratorService.checkHeaterConfig(
        this.activatedRoute.snapshot.params['itemId'],
        this.activatedRoute.snapshot.params['connectionId'],
        this.activatedRoute.snapshot.params['colorId'],
        this.activatedRoute.snapshot.params['heaterItemId'],
        this.activatedRoute.snapshot.params['heaterConfigId']);
    } else {
      this.configuratorService.checkConfig(
        this.activatedRoute.snapshot.params['itemId'],
        this.activatedRoute.snapshot.params['connectionId'],
        this.activatedRoute.snapshot.params['colorId']);
    }
    this.API.configuratorPriceGetItemIdConfigIdGetWithHttpInfo(
      this.activatedRoute.snapshot.params['itemId'],
      'K' + this.activatedRoute.snapshot.params['colorId'] + this.activatedRoute.snapshot.params['connectionId'],
      AuthService.getRequestParams()
    )
      .subscribe(
        data => {
          console.log('configuratorPriceItemIdConfigId', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            let priceResp = <PriceCustomerResponseTER> data.json();
            this.price = priceResp.salesPrice;
            this.configuratorService.setPrice(this.price);
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public addToCart() {
    this.cartService.setConfig(this.configuratorService.getConfiguration());
    this.cartService.addToCart(
      {
        modelName: this.configuratorService.getLabel(),
        itemId: 0,
        sku: this.configuratorService.getSKU(),
        quantity: this.quantity,
        amount: this.configuratorService.getPrice(),
        currencyCode: 'PLN',
        data: '',
        deleted: 0
      }
    );
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
