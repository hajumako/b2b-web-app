import { Component, InjectionToken } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { StepModel } from '../../models/step.model';
import { AuthService } from '../../services/auth.service';
import { CacheService } from '../../services/cache.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService, ResponseCode, RadiatorConnections } from '../../api';

const STEP = 1;
const CACHE_CONF_CONN = new InjectionToken<string>('cache_conf_connections');

@Component({
  styleUrls: ['./connections.component.scss'],
  templateUrl: './connections.component.html'
})
export class ConnectionsComponent extends CachedComponent {
  public connections: RadiatorConnections[];
  public connectionId: string = '';
  public showLegend: boolean = false;

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.backNav) {
      this.connections = this.cacheService.getCache(CACHE_CONF_CONN.toString());
      this.connectionId = this.configuratorService.getConnection();
    } else {
      this.configuratorService.checkItemId(activatedRoute.snapshot.params['itemId']);
      this.API.configuratorConnectionsItemIdGetWithHttpInfo(activatedRoute.snapshot.params['itemId'], AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('configuratorConnectionsItemId', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.connections = data.json();
              this.cacheService.saveCache(CACHE_CONF_CONN.toString(), this.connections);
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    }
  }

  public setConnection(index: number) {
    this.connectionId = this.connections[index].connectionId;
    this.configuratorService.setConnection(this.connections[index].connectionId);
    this.configuratorService.saveConfiguration();
  }

  public goBack() {
    this.location.back();
  }
}
