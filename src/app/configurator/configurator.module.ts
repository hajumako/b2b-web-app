import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../pipes/pipes.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { routes } from './configurator.routes';
import { ConfiguratorComponent } from './configurator.component';
import { ModelsComponent } from './models/models.component';
import { SizesComponent } from './sizes/sizes.component';
import { CoatingsComponent } from './coatings/coatings.component';
import { ConnectionsComponent } from './connections/connections.component';
import { SummaryComponent } from './summary/summary.component';
import { _DevComponentsModule } from '../_dev-components/_dev-components.module';
import { HeaterModelsComponent } from './heater-models/heater-models.component';
import { HeaterConfigsComponent } from './heater-configs/heater-configs.component';

@NgModule({
  declarations: [
    ConfiguratorComponent,
    ModelsComponent,
    SizesComponent,
    ConnectionsComponent,
    CoatingsComponent,
    SummaryComponent,
    HeaterModelsComponent,
    HeaterConfigsComponent
  ],
  exports: [],
  imports: [
    PipesModule,
    SharedComponentsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    _DevComponentsModule,
    RouterModule.forChild(routes)
  ],
})
export class ConfiguratorModule {
  public static routes = routes;
}
