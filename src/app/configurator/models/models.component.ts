import { AfterViewInit, Component, InjectionToken, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CacheService } from '../../services/cache.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { DevData } from '../../interfaces/devData';
import { ProductGroupModel } from '../../models/product-group.model';
import { StepModel } from '../../models/step.model';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService, ResponseCode, ProductGroupId, RadiatorModel } from '../../api';
import { ActivatedRoute } from '@angular/router';

const STEP = -1;
const CACHE_CONF_MODELS = new InjectionToken<string>('cache_conf_models');
const CACHE_ACTIVE_TAB = new InjectionToken<string>('cache_active_tab');

@Component({
  styleUrls: ['./models.component.scss'],
  templateUrl: './models.component.html'
})
export class ModelsComponent extends CachedComponent implements OnInit, AfterViewInit, OnDestroy {
  public activeTab: number = 0;

  public loadingArr: boolean[] = [
    false,
    false,
    false,
    false
  ];

  public x;

  public productGroups: ProductGroupModel[] = [
    {
      id: ProductGroupId.WYRG.toString(),
      name: 'Profilowe',
      models: []
    },
    {
      id: ProductGroupId.WYRZ.toString(),
      name: 'Odlewane',
      models: []
    },
    {
      id: ProductGroupId.WYRE.toString(),
      name: 'Grzałki',
      models: []
    },
    {
      id: ProductGroupId.WYRR.toString(),
      name: 'Akcesoria',
      models: []
    }
  ];

  private _alive: boolean = true;
  private _fragment: string = null;

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private route: ActivatedRoute,
              private API: DefaultService,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.backNav) {
      this.productGroups = <ProductGroupModel[]> this.cacheService.getCache(CACHE_CONF_MODELS.toString());
      this.activeTab = <number> this.cacheService.getCache(CACHE_ACTIVE_TAB.toString());
    } else {
      this.activateTab(this.activeTab);
    }
  }

  public ngOnInit(): any {
    this.route.fragment.subscribe(fragment => {
      this._fragment = fragment;
    });
  }

  public ngAfterViewInit(): void {
    try {
      switch (this._fragment) {
        case 'profile':
          this.activateTab(0);
          break;
        case 'cast-iron':
          this.activateTab(1);
          break;
        case 'heaters':
          this.activateTab(2);
          break;
        case 'accessories':
          this.activateTab(3);
          break;
        default:
          this.activateTab(0);
      }
    } catch (e) {
      console.log('ModelsComponent error', e);
    }
  }

  public activateTab(i: number) {
    this.activeTab = i;
    this.cacheService.saveCache(CACHE_ACTIVE_TAB.toString(), this.activeTab);
    if (!this.productGroups[i].models.length) {
      this.loadingArr[i] = true;
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorModelsGroupedGetWithHttpInfo(AuthService.getRequestParams())
        .subscribe(
          data => {
            this.loadingArr[this.activeTab] = false;
            console.log('configuratorModelsGrouped', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.productGroups[0].models = data.json();
              this.cacheService.saveCache(CACHE_CONF_MODELS.toString(), this.productGroups);
            }
          },
          error => {
            this.loadingArr[this.activeTab] = false;
            this.eventEmitter.error(error);
          }
        );
    }
  }

  public setGroupAndModel(productGroup: string, model: RadiatorModel) {
    this.configuratorService.setProductGroup(productGroup);
    this.configuratorService.setModel(model.radiatorModel);
    this.configuratorService.setLabel(model.labelName);
    this.configuratorService.setImage(model.image);
    this.configuratorService.setImages(null);
    this.configuratorService.setColor(null);
    this.configuratorService.saveConfiguration();
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  public key0(obj: Object) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        return key;
      }
    }
  }
}
