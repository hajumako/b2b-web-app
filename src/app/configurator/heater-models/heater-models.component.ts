import { Component, InjectionToken } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { StepModel } from '../../models/step.model';
import { AuthService } from '../../services/auth.service';
import { CacheService } from '../../services/cache.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService } from '../../api';
import { MockDataService } from '../../services/mock-data.service';
import { ResponseCode } from '../../api/model/responseCode';
import { ItemIdConfigIdContractTER } from '../../api/model/itemIdConfigIdContractTER';

const STEP = 3;
const CACHE_CONF_HEAT_MODELS = new InjectionToken<string>('cache_conf_heater_models');

@Component({
  styleUrls: ['./heater-models.component.scss'],
  templateUrl: './heater-models.component.html'
})
export class HeaterModelsComponent extends CachedComponent {
  public heaters: any;
  public heaterItemId: string;

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService,
              private mockDataService: MockDataService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.backNav) {
      this.heaters = this.cacheService.getCache(CACHE_CONF_HEAT_MODELS.toString());
    } else {
      this.configuratorService.checkConfig(
        this.activatedRoute.snapshot.params['itemId'],
        this.activatedRoute.snapshot.params['connectionId'],
        this.activatedRoute.snapshot.params['colorId']);
      this.API.configuratorRadiatorHeatersItemIdConfigIdGetWithHttpInfo(
        this.activatedRoute.snapshot.params['itemId'],
        'K' + this.activatedRoute.snapshot.params['colorId'] +
        this.activatedRoute.snapshot.params['connectionId'], AuthService.getRequestParams()
      )
        .subscribe(
          data => {
            console.log('configuratorRadiatorHeatersItemIdConfigId', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.heaters = <ItemIdConfigIdContractTER[]> data.json();
              this.cacheService.saveCache(CACHE_CONF_HEAT_MODELS.toString(), this.heaters);
            }

          },
          error => {
            this.eventEmitter.error(error);
          }
        );
      /*this.mockDataService.getData('heaterModels')
        .subscribe(
          data => {
            this.heaters = data;
            this.cacheService.saveCache(CACHE_CONF_HEAT_MODELS.toString(), this.heaters);
          }
        );*/
    }
  }

  public setHeater(heater: any) {
    this.heaterItemId = heater.itemId;
    this.configuratorService.setHeaterModel(heater);
    this.configuratorService.saveHeaterConfiguration();
  }

  public goBack() {
    this.location.back();
  }
}
