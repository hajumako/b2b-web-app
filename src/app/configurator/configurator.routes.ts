import { AuthGuard } from '../shared/auth.guard';
import { ConfiguratorComponent } from './configurator.component';
import { ModelsComponent } from './models/models.component';
import { SizesComponent } from './sizes/sizes.component';
import { ConnectionsComponent } from './connections/connections.component';
import { CoatingsComponent } from './coatings/coatings.component';
import { SummaryComponent } from './summary/summary.component';
import { HeaterConfigsComponent } from './heater-configs/heater-configs.component';
import { HeaterModelsComponent } from './heater-models/heater-models.component';

export const routes = [
  {
    path: '', component: ConfiguratorComponent, children: [
    {path: '', pathMatch: 'full', redirectTo: 'stockModels'},
    {path: 'stockModels', component: ModelsComponent, canActivate: [AuthGuard]},
    {path: ':model/size', component: SizesComponent, canActivate: [AuthGuard]},
    {path: ':itemId/connection', component: ConnectionsComponent, canActivate: [AuthGuard]},
    {path: ':itemId/:connectionId/coating', component: CoatingsComponent, canActivate: [AuthGuard]},
    {path: ':itemId/:connectionId/:colorId/summary', component: SummaryComponent, canActivate: [AuthGuard]},
    {path: ':itemId/:connectionId/:colorId/heater', component: HeaterModelsComponent, canActivate: [AuthGuard]},
    {
      path: ':itemId/:connectionId/:colorId/:heaterItemId/config',
      component: HeaterConfigsComponent,
      canActivate: [AuthGuard]
    },
    {
      path: ':itemId/:connectionId/:colorId/:heaterItemId/:heaterConfigId/summary',
      component: SummaryComponent,
      canActivate: [AuthGuard]
    },
  ]
  },
];
