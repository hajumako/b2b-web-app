import { Component, InjectionToken } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { CacheService } from '../../services/cache.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { StepModel } from '../../models/step.model';
import { CachedComponent } from '../../shared/cached.component';
import {
  DefaultService,
  Image,
  ColorGroup,
  RadiatorColors,
  ResponseCode,
  RadiatorCoatingsStructTER,
  ProductGroupId,
  NoYes,
  CoatingTypeInterface
} from '../../api';
import { ProductGroupIdShort } from '../../api/model/productGroupIdShort';

const STEP: number = 2;
const CACHE_CONF_COLO = new InjectionToken<string>('cache_conf_colors');
const CACHE_CONF_COAT = new InjectionToken<string>('cache_conf_coatings');

@Component({
  styleUrls: ['./coatings.component.scss'],
  templateUrl: './coatings.component.html'
})
export class CoatingsComponent extends CachedComponent {
  public colorGroups: ColorGroup[];
  public possibleColors: string[];
  public possibleCoatings: RadiatorCoatingsStructTER;
  public colorId: string = '';
  public chosenGroup: number = null;
  public loading: boolean = true;
  public nextStep: string = 'summary';

  public hideEmptyGroups: boolean = false;
  public hideNotExistingConfigs: boolean = false;

  protected loadingArr: boolean[] = [
    true,
    true
  ];

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.configuratorService.needsHeater()) {
      this.nextStep = 'heater';
    }
    this.colorGroups = this.configuratorService.getColorGroups();
    this.colorId = this.configuratorService.getColor() ? this.configuratorService.getColor().colorId : null;
    if (this.backNav) {
      this.possibleColors = <string[]> this.cacheService.getCache(CACHE_CONF_COLO.toString());
      this.possibleCoatings = <string[]> this.cacheService.getCache(CACHE_CONF_COAT.toString());
      this.loading = false;
    } else {
      this.configuratorService.checkConnection(this.activatedRoute.snapshot.params['itemId'], this.activatedRoute.snapshot.params['connectionId']);
      this.API.configuratorColorsShortItemIdConnectionIdGetWithHttpInfo(
        this.activatedRoute.snapshot.params['itemId'],
        this.activatedRoute.snapshot.params['connectionId'],
        AuthService.getRequestParams()
      )
        .subscribe(
          data => {
            this.loadingArr[0] = false;
            this.loading = this.isStillLoading();
            console.log('configuratorColorsShortItemIdConnectionId', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.possibleColors = <string[]> data.json();
              this.cacheService.saveCache(CACHE_CONF_COLO.toString(), this.possibleColors);
            }
          },
          error => {
            this.loading = false;
            this.eventEmitter.error(error);
          }
        );
      this.API.configuratorCoatingsItemIdGetWithHttpInfo(
        this.activatedRoute.snapshot.params['itemId'],
        AuthService.getRequestParams()
      )
        .subscribe(
          data => {
            this.loadingArr[1] = false;
            this.loading = this.isStillLoading();
            console.log('configuratorCoatingsItemId', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.possibleCoatings = <RadiatorCoatingsStructTER> data.json();
              this.cacheService.saveCache(CACHE_CONF_COAT.toString(), this.possibleCoatings);
            }
          },
          error => {
            this.loading = false;
            this.eventEmitter.error(error);
          }
        );
    }
  }

  public setColorGroup(index: number) {
    this.chosenGroup = index;
  }

  public setColor(color: RadiatorColors) {
    this.colorId = color.colorId;
    this.configuratorService.setColor(color);
    this.configuratorService.saveConfiguration();
    this.API.defaultHeaders = this.authService.getHeaders();
    this.API.imageItemIdConfigIdGetWithHttpInfo(this.configuratorService.getItemId(), this.configuratorService.getConfigId(), AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('imageItemIdConfigIdGet', data);
          this.configuratorService.setImage(<Image> data.json());
          this.configuratorService.saveConfiguration();
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public isColorPossible(colorId: string): boolean {
    return this.possibleColors.indexOf(colorId) > -1;
  }

  public isCoatingTypePossible(coatingType: string) {
    if (coatingType === CoatingTypeInterface.Iron.toString()) {
      if (this.configuratorService.getProductGroup() === ProductGroupId.WYRZ.toString()) {
        if (this.possibleCoatings.paint === NoYes.Yes) {
          return true;
        }
      }
    } else if (coatingType === CoatingTypeInterface.Powder.toString()) {
      if (this.possibleCoatings.paint === NoYes.Yes) {
        return true;
      }
    } else if (coatingType === CoatingTypeInterface.Galvanic.toString()) {
      if (this.possibleCoatings.galvanic === NoYes.Yes) {
        return true;
      }
    }
    return false;
  }

  public hasPossibleColorsInGroup(i: number): boolean {
    return this.colorGroups[i].colors.some(color => {
      if (this.isColorPossible(color.colorId)) {
        return true;
      }
    });
  }

  public goBack() {
    this.location.back();
  }

  protected isStillLoading() {
    return this.loadingArr.some(l => {
      if (l) {
        return true;
      }
    });
  }
}
