import { Component, InjectionToken } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { StepModel } from '../../models/step.model';
import { AuthService } from '../../services/auth.service';
import { CacheService } from '../../services/cache.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService } from '../../api';
import { MockDataService } from '../../services/mock-data.service';

const STEP = 4;
const CACHE_CONF_HEAT_COLORS = new InjectionToken<string>('cache_conf_heater_colors');
const CACHE_CONF_HEAT_CABLES = new InjectionToken<string>('cache_conf_heater_cables');

@Component({
  styleUrls: ['./heater-configs.component.scss'],
  templateUrl: './heater-configs.component.html'
})
export class HeaterConfigsComponent extends CachedComponent {
  public colors: any;
  public cables: any;
  public heaterColorId: string;
  public heaterCableId: string;
  public heaterConfigId: string;

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService,
              private mockDataService: MockDataService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.backNav) {
      this.colors = this.cacheService.getCache(CACHE_CONF_HEAT_COLORS.toString());
      this.cables = this.cacheService.getCache(CACHE_CONF_HEAT_CABLES.toString());
    } else {
      this.configuratorService.checkHeaterModel(
        this.activatedRoute.snapshot.params['itemId'],
        this.activatedRoute.snapshot.params['connectionId'],
        this.activatedRoute.snapshot.params['colorId'],
        this.activatedRoute.snapshot.params['heaterItemId']);
      this.mockDataService.getData('heaterColors')
        .subscribe(
          data => {
            this.colors = data['WEMOA01'];
            this.cacheService.saveCache(CACHE_CONF_HEAT_COLORS.toString(), this.colors);
          }
        );
      this.mockDataService.getData('heaterCables')
        .subscribe(
          data => {
            this.cables = data['WEMOA01'];
            this.cacheService.saveCache(CACHE_CONF_HEAT_CABLES.toString(), this.cables);
          }
        );
    }
  }

  public setHeaterColor(color: any) {
    this.heaterColorId = color.colorId;
    this.configuratorService.setHeaterColorId(color.colorId);
    this.configuratorService.saveHeaterConfiguration();

    this.heaterConfigId = this.configuratorService.getHeaterConfigId();
  }

  public setHeaterCable(cable: any) {
    this.heaterCableId = cable.cableId;
    this.configuratorService.setHeaterCableId(cable.cableId);
    this.configuratorService.saveHeaterConfiguration();

    this.heaterConfigId = this.configuratorService.getHeaterConfigId();
  }

  public goBack() {
    this.location.back();
  }
}
