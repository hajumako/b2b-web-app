import { AfterViewInit, Component, InjectionToken } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { DimensionsPipe } from '../../pipes/dimensions.pipe';
import { AuthService } from '../../services/auth.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { CacheService } from '../../services/cache.service';
import { ConfiguratorService } from '../../services/configurator.service';
import { StepModel } from '../../models/step.model';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService, ResponseCode, RadiatorSize, Image } from '../../api';

const STEP = 0;
const CACHE_CONF_SIZES = new InjectionToken<string>('cache_conf_sizes');
const CACHE_CONF_DIMS = new InjectionToken<string>('cache_conf_dims');

@Component({
  styleUrls: ['./sizes.component.scss'],
  templateUrl: './sizes.component.html'
})
export class SizesComponent extends CachedComponent implements AfterViewInit {
  public sizes: RadiatorSize[];
  public itemId: string = null;
  public loading: boolean = true;

  public scrollTarget;

  public objectKeys = Object.keys;

  public listView: boolean = true;
  public chosenHeight: number;
  public chosenWidth: number;

  public dims = {};
  public configExists: boolean = true;

  public selectedIndex = 0;
  public angle = 0;
  public imagesBase = 18;
  public rotationBase = 20;

  constructor(public configuratorService: ConfiguratorService,
              private authService: AuthService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.CONFIGURATOR,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.backNav) {
      this.sizes = <RadiatorSize[]> this.cacheService.getCache(CACHE_CONF_SIZES.toString());
      this.dims = this.cacheService.getCache(CACHE_CONF_DIMS.toString());
      this.itemId = this.configuratorService.getItemId();
      this.chosenHeight = this.configuratorService.getHeight();
      this.chosenWidth = this.configuratorService.getWidth();
      this.loading = false;
    } else {
      this.configuratorService.checkModel(activatedRoute.snapshot.params['model']);
      this.API.defaultHeaders = this.authService.getHeaders();
      this.API.configuratorSizesModelGetWithHttpInfo(activatedRoute.snapshot.params['model'], AuthService.getRequestParams())
        .subscribe(
          data => {
            this.loading = false;
            console.log('configuratorSizesModelGet', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.sizes = <RadiatorSize[]> data.json();
              this.sizes.forEach((size, i) => {
                let dP = new DimensionsPipe();
                let h = Number.parseInt(dP.transform(size.name, 'height', '/', ''));
                let w = Number.parseInt(dP.transform(size.name, 'width', '/', ''));
                if (this.dims && this.dims.hasOwnProperty(h)) {
                  this.dims[h].push([w, i]);
                } else {
                  this.dims[h] = [[w, i]];
                }
              });
              this.cacheService.saveCache(CACHE_CONF_SIZES.toString(), this.sizes);
              this.cacheService.saveCache(CACHE_CONF_DIMS.toString(), this.dims);
            }
          },
          error => {
            this.loading = false;
            this.eventEmitter.error(error);
          }
        );
    }
  }

  public ngAfterViewInit() {
    this.scrollTarget = document.getElementById('sizes-list');
  }

  public rotateCarousel() {
    this.angle = -this.selectedIndex * this.rotationBase;
  }

  public prev() {
    if (this.selectedIndex < this.configuratorService.getImages().length - 1) {
      this.selectedIndex++;
      this.rotateCarousel();
    }
  }

  public next() {
    if (this.selectedIndex > 0) {
      this.selectedIndex--;
      this.rotateCarousel();
    }
  }

  public setHeight(h: number) {
    this.chosenHeight = h;
    this.chosenWidth = null;
    this.itemId = null;
  }

  public setWidth(w: number[]) {
    this.chosenWidth = w[0];
    this.setItemIdAndName(w[1]);
    console.log(w);
  }

  public setSize(index: number) {
    let dP = new DimensionsPipe();
    this.chosenHeight = Number.parseInt(dP.transform(this.sizes[index].name, 'height', '/', ''));
    this.chosenWidth = Number.parseInt(dP.transform(this.sizes[index].name, 'width', '/', ''));
    this.setItemIdAndName(index);
  }

  public goBack() {
    this.location.back();
  }

  protected setItemIdAndName(index: number) {
    this.configExists = this.sizes[index].configExists;
    this.itemId = this.sizes[index].itemId;
    this.configuratorService.setItemId(this.sizes[index].itemId);
    this.configuratorService.setName(this.sizes[index].name);
    this.configuratorService.setImage(this.sizes[index].image);
    this.configuratorService.setImages(this.sizes[index].images);
    this.configuratorService.setConnection('');
    this.configuratorService.saveConfiguration();
  }
}
