import { AuthGuard } from '../shared/auth.guard';
import { AccountComponent } from './account.component';
import { DetailsComponent } from './details/details.component';
import { MainComponent } from './main/main.component';
import { OrdersComponent } from './orders/orders.component';
import { DefaultDataComponent } from './default-data/default-data.component';

export const routes = [
  {
    path: '', component: AccountComponent, children: [
    {path: '', pathMatch: 'full', redirectTo: 'main'},
    {path: 'main', component: MainComponent, canActivate: [AuthGuard]},
    {path: 'default-data', component: DefaultDataComponent, canActivate: [AuthGuard]},
    {path: 'orders', component: OrdersComponent, canActivate: [AuthGuard]},
    {path: 'order/:orderIdAX', component: DetailsComponent, canActivate: [AuthGuard]}
  ]
  },
];
