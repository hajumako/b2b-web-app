import { Component, InjectionToken, Input } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DefaultService } from '../api/api/default.service';
import { CachedComponent } from '../shared/cached.component';
import { EventEmitterService } from '../services/event-emitter.service';
import { CacheService } from '../services/cache.service';
import { DtOutletCustomerRequest } from '../api/model/dtOutletCustomerRequest';
import { Order } from '../api/model/order';
import { ResponseCode } from '../api/model/responseCode';
import { SalesTableContractTER } from '../api/model/salesTableContractTER';
import { SalesTableContract } from '../api/model/salesTableContract';

const ORDERS = new InjectionToken<string>('orders');
const ACCOUNT = new InjectionToken<string>('account');

@Component({
  styleUrls: ['./account.component.scss'],
  templateUrl: 'account.component.html'
})
export class AccountComponent extends CachedComponent {
  public title = 'Moje konto';

  public orders: SalesTableContract[];
  public account: DtOutletCustomerRequest;

  constructor(private authService: AuthService,
              private API: DefaultService,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);

    if (this.backNav) {
      this.orders = JSON.parse(sessionStorage.getItem(ORDERS.toString()));
      this.account = JSON.parse(sessionStorage.getItem(ACCOUNT.toString()));
    } else {
      API.accountOrdersAxListGetWithHttpInfo(5, 0, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('accountOrdersListAX', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.orders = <SalesTableContract[]> data.json();
              sessionStorage.setItem(ORDERS.toString(), JSON.stringify(this.orders));
            }
          },
          error => {
            this.eventEmitter.error(error);
          });
/*      API.accountOrdersListGetWithHttpInfo(null, 'orderId.desc', null, null, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('orders', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.orders = <Order[]> data.json();
              sessionStorage.setItem(ORDERS.toString(), JSON.stringify(this.orders));
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );*/
      /*API.accountGetWithHttpInfo(AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('account info', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.account = <DtOutletCustomerRequest> data.json();
              sessionStorage.setItem(ACCOUNT.toString(), JSON.stringify(this.account));
            }
          },
          error => {
            console.log('err', error);
            if (error.status === ResponseCode.NUMBER_401) {
              this.authService.logout();
            }
          }
        );*/
    }
  }
}
