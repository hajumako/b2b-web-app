import { Component, InjectionToken } from '@angular/core';
import { DefaultService } from '../../api/api/default.service';
import { AuthService } from '../../services/auth.service';
import { ResponseCode } from '../../api/model/responseCode';
import { DtOutletCustomerRequest } from '../../api/model/dtOutletCustomerRequest';
import { EventEmitterService } from '../../services/event-emitter.service';
import { CacheService } from '../../services/cache.service';
import { CachedComponent } from '../../shared/cached.component';
import { AppState } from '../../app.service';
import { SalesTableContract } from '../../api/model/salesTableContract';

const ORDERS = new InjectionToken<string>('orders');
const ORDERS_T = new InjectionToken<string>('orders_t');
const CACHE_PAGE = new InjectionToken<string>('orders_page');

@Component({
  styleUrls: ['./orders.component.scss'],
  templateUrl: 'orders.component.html'
})
export class OrdersComponent extends CachedComponent {
  public title = 'Zamówienia';

  public orders: SalesTableContract[];

  public limit: number;
  public total: number = 0;
  public currentPage: number = 1;

  constructor(private authService: AuthService,
              private API: DefaultService,
              private appState: AppState,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    this.limit = appState.get('limit');

    if (this.backNav) {
      this.orders = JSON.parse(sessionStorage.getItem(ORDERS.toString()));
      this.total = JSON.parse(sessionStorage.getItem(ORDERS_T.toString()));
      this.currentPage = JSON.parse(sessionStorage.getItem(CACHE_PAGE.toString()));
    } else {
      // get orders
      this.getOrdersPage(this.currentPage);
    }
  }

  public getOrdersPage(page: number) {
    this.orders = null;
    this.API.accountOrdersAxListGetWithHttpInfo(
      this.limit,
      this.limit * (page - 1),
      AuthService.getRequestParams())
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            if (data.headers.has('X-Total')) {
              this.total = Number(data.headers.get('X-Total'));
              this.cacheService.saveCache(ORDERS_T.toString(), this.total);
            }
            this.orders = <SalesTableContract[]> data.json();
            this.currentPage = page;
            this.cacheService.saveCache(CACHE_PAGE.toString(), page);
            sessionStorage.setItem(ORDERS.toString(), JSON.stringify(this.orders));
          }
        },
        error => {
          this.eventEmitter.error(error);
        });
  }
}
