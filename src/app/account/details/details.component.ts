import { Component, InjectionToken, OnInit } from '@angular/core';
import { DefaultService } from '../../api/api/default.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ResponseCode } from '../../api/model/responseCode';
import { EventEmitterService } from '../../services/event-emitter.service';
import { SalesTableContract } from '../../api/model/salesTableContract';
import { CartService } from '../../services/cart.service';

declare let $: any;

@Component({
  styleUrls: ['./details.component.scss'],
  templateUrl: 'details.component.html'
})
export class DetailsComponent implements OnInit {
  public title = 'Zamówienie ';

  public orderIdAX: string;
  public order: SalesTableContract;
  public configs: Object;
  public modalId: string = 'orderModal';
  
  public total: number = 0;

  constructor(private authService: AuthService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private eventEmitter: EventEmitterService,
              private cartService: CartService,
              private router: Router) {
    this.orderIdAX = activatedRoute.snapshot.params['orderIdAX'];

    API.accountOrdersAxItemOrderIdAXGetWithHttpInfo(this.orderIdAX, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountOrdersAxItemOrderIdAX', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.order = <SalesTableContract> data.json();
            this.getConfigs();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public addToCart(checkCart: boolean, replace: boolean) {
    if (checkCart && this.cartService.cartCnt) {
      $('#' + this.modalId).foundation('open');
      return;
    }
    if (replace) {
      this.cartService.clearCart();
    }
    let lineItems = [];
    this.order.salesLineItems.forEach((salesLineItem) => {
      lineItems.push({
        modelName: salesLineItem.parmItemName,
        itemId: 0,
        sku: salesLineItem.sku,
        quantity: salesLineItem.parmSalesQty,
        amount: salesLineItem.parmSalesPrice,
        currencyCode: 'PLN',
        data: '',
        deleted: 0
      });
    });
    this.cartService.addToCartBulk(lineItems);
    this.router.navigate(['/cart']);
  }

  public ngOnInit() {
    $(document).foundation();
  }

  private getConfigs() {
    let skus = [];
    this.order.salesLineItems.forEach((lineItem) => {
      skus.push(lineItem.sku);
      this.total += lineItem.parmLineAmount;
    });
    console.log(this.total, this.order.parmTotalAmount);
    this.cartService.getConfigs(skus)
      .subscribe(
        data => {
          this.configs = data;
          console.log('configs:', this.configs);
        });
  }
}
