import { Component, InjectionToken } from '@angular/core';
import { DefaultService } from '../../api/api/default.service';
import { AuthService } from '../../services/auth.service';
import { ResponseCode } from '../../api/model/responseCode';
import { DtOutletCustomerRequest } from '../../api/model/dtOutletCustomerRequest';
import { EventEmitterService } from '../../services/event-emitter.service';
import { CacheService } from '../../services/cache.service';
import { CachedComponent } from '../../shared/cached.component';
import { DeliveryType } from '../../api/model/deliveryType';
import { PaymentMethod } from '../../api/model/paymentMethod';
import { SalesTableContract } from '../../api/model/salesTableContract';

const ACCOUNT = new InjectionToken<string>('account');
const DELIVERY = new InjectionToken<string>('delivery');
const PAYMENT = new InjectionToken<string>('payment');
const ORDERS = new InjectionToken<string>('orders');

@Component({
  styleUrls: ['./main.component.scss'],
  templateUrl: 'main.component.html'
})
export class MainComponent extends CachedComponent {
  public title: string = 'Moje konto';

  public account: DtOutletCustomerRequest;
  public deliveryType: DeliveryType;
  public paymentMethod: PaymentMethod;
  public orders: SalesTableContract[];

  constructor(private authService: AuthService,
              private API: DefaultService,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);

    if (this.backNav) {
      this.account = JSON.parse(sessionStorage.getItem(ACCOUNT.toString()));
      this.deliveryType = JSON.parse(sessionStorage.getItem(DELIVERY.toString()));
      this.paymentMethod = JSON.parse(sessionStorage.getItem(PAYMENT.toString()));
      this.orders = JSON.parse(sessionStorage.getItem(ORDERS.toString()));
    } else {
      API.accountGetWithHttpInfo(AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('account', data);
            this.authService.checkTokenHeader(data.headers);
            if (data.status === ResponseCode.NUMBER_200) {
              this.account = <DtOutletCustomerRequest> data.json();
              sessionStorage.setItem(ACCOUNT.toString(), JSON.stringify(this.account));
            }
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    }

    API.accountDeliverytypeGetWithHttpInfo(AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountDeliverytype', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.deliveryType = <DeliveryType> data.json();
            sessionStorage.setItem(DELIVERY.toString(), JSON.stringify(this.deliveryType));
          }
        },
        error => {
          this.eventEmitter.error(error);
        });

    API.accountPaymentmethodGetWithHttpInfo(AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountPaymentmethod', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.paymentMethod = <PaymentMethod> data.json();
            sessionStorage.setItem(PAYMENT.toString(), JSON.stringify(this.paymentMethod));
          }
        },
        error => {
          this.eventEmitter.error(error);
        });

    API.accountOrdersAxListGetWithHttpInfo(5, 0, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountOrdersListAX', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.orders = <SalesTableContract[]> data.json();
            sessionStorage.setItem(ORDERS.toString(), JSON.stringify(this.orders));
          }
        },
        error => {
          this.eventEmitter.error(error);
        });
  }
}
