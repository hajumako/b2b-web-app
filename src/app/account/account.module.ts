import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { routes } from './account.routes';
import { PipesModule } from '../pipes/pipes.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { AccountComponent } from './account.component';
import { DetailsComponent } from './details/details.component';
import { OrdersComponent } from './orders/orders.component';
import { MainComponent } from './main/main.component';
import { DefaultDataComponent } from './default-data/default-data.component';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { PrepareIdPipe } from '../pipes/prepareid.pipe';
import { _DevComponentsModule } from '../_dev-components/_dev-components.module';
import 'foundation-sites/dist/js/foundation.min';

@NgModule({
  declarations: [
    AccountComponent,
    DetailsComponent,
    MainComponent,
    DefaultDataComponent,
    OrdersComponent,
    OrdersListComponent
  ],
  exports: [],
  imports: [
    SharedComponentsModule,
    PipesModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
    _DevComponentsModule
  ],
  providers: [
    PrepareIdPipe
  ]
})
export class AccountModule {
  public static routes = routes;
}
