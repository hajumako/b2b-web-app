import { Component, EventEmitter, Input, OnChanges, Output, SimpleChange } from '@angular/core';
import { SalesTableContractTER } from '../../api/model/salesTableContractTER';
import { AppState } from '../../app.service';

@Component({
  selector: 'orders-list',
  styleUrls: ['./orders-list.component.scss'],
  templateUrl: './orders-list.component.html'
})
export class OrdersListComponent {
  @Input() public orders: SalesTableContractTER[];

  @Output() public onOpenDetails = new EventEmitter<string>();

  constructor(private appState: AppState) {
    
  }

  public openDetails(index: string): void {
    this.onOpenDetails.emit(index);
  }
}
