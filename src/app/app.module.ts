import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule, ApplicationRef } from '@angular/core';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { routing } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
// api
import { ApiModule } from './api/api.module';
import { APIurl } from './shared/APIurl';
// auth guard
import { AuthGuard } from './shared/auth.guard';
// directives
import { OnlyNumberDirective } from './directives/only-number.directive';
import { MinMaxDirective } from './directives/min-max.directive';
// services
import { AuthService } from './services/auth.service';
import { CartService } from './services/cart.service';
import { PagerService } from './services/pager.service';
import { EventEmitterService } from './services/event-emitter.service';
import { CacheService } from './services/cache.service';
import { ParserService } from './services/parser.service';
import { ConfiguratorService } from './services/configurator.service';
import { MockDataService } from './services/mock-data.service';
// styles
import '../styles/styles.scss';
// js
import 'foundation-sites/dist/js/foundation.min';
// modules
import { PipesModule } from './pipes';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedComponentsModule } from './shared-components';
// subcomponents
import { HeaderComponent } from './shared/header.component';
import { FooterComponent } from './shared/footer.component';
// components
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WarehouseComponent } from './warehouse/warehouse/warehouse.component';
import { WarehouseDetailsComponent } from './warehouse/details/warehouseDetails.component';
import { CartComponent } from './cart/cart.component';
import { CachedComponent } from './shared/cached.component';
import { HeaterComponent } from './details/heater.component';
import { RadiatorComponent } from './details/radiator.component';
import { AccessoryComponent } from './details/accessory.component';
import { ErrorModalComponent } from './shared/error-modal/error-modal.component';
import { NoContentComponent } from './no-content';
// pipes
import { DimensionsPipe } from './pipes/dimensions.pipe';

// dev stuff
import { _DevComponentsModule } from './_dev-components/';
import { AXWarehouseComponent } from './warehouse/ax-warehouse/ax-warehouse.component';
import { InfoModalComponent } from './shared/info-modal/info-modal.component';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  AuthGuard,
  AuthService,
  CacheService,
  CartService,
  EventEmitterService,
  PagerService,
  ConfiguratorService,
  ParserService,
  DimensionsPipe,
  APIurl,
  MockDataService
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    // directives
    OnlyNumberDirective,
    MinMaxDirective,

    // subcomponents
    FooterComponent,
    HeaderComponent,
    ErrorModalComponent,
    InfoModalComponent,

    // components
    AppComponent,
    LoginComponent,
    DashboardComponent,
    WarehouseComponent,
    AXWarehouseComponent,
    WarehouseDetailsComponent,
    CartComponent,
    CachedComponent,
    HeaterComponent,
    RadiatorComponent,
    AccessoryComponent,
    NoContentComponent,
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    ApiModule,
    PipesModule,
    SharedComponentsModule,
    LazyLoadImageModule,
    routing,

    _DevComponentsModule
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {

  constructor(public appRef: ApplicationRef,
              public appState: AppState) {
  }

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    /**
     * Set state
     */
    this.appState._state = store.state;
    /**
     * Set input values
     */
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    /**
     * Save state
     */
    const state = this.appState._state;
    store.state = state;
    /**
     * Recreate root elements
     */
    store.disposeOldHosts = createNewHosts(cmpLocation);
    /**
     * Save input values
     */
    store.restoreInputValues = createInputTransfer();
    /**
     * Remove styles
     */
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    /**
     * Display new elements
     */
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
