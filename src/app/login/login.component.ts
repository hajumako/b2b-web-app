import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { EventEmitterService } from '../services/event-emitter.service';
import { DefaultService } from '../api';
import { ResponseCode } from '../api/model/responseCode';

@Component({
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public errorCode = null;
  public errorMessage = null;

  public busy: boolean = false;

  private _user: string = ('production' !== ENV) ? TEST_USER : '';
  private _password: string = ('production' !== ENV) ? TEST_PASS : '';

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private API: DefaultService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private eventEmitter: EventEmitterService) {
    if (authService.isLoggedIn) {
      router.navigate(['/configurator']);
    }
    if (this.activatedRoute.snapshot.queryParams.hasOwnProperty('redirect')) {
      this.authService.setRedirectURL(this.activatedRoute.snapshot.queryParams['redirect']);
    }
  }

  public onSignin() {
    if (this.loginForm.valid && !this.busy) {
      this.busy = true;
      this.errorCode = null;
      this.errorMessage = null;
      this.API.loginPostWithHttpInfo(this.loginForm.value, AuthService.getRequestParams()).subscribe(
        data => {
          this.busy = false;
          this.authService.afterLogin(data.headers);
        },
        error => {
          this.busy = false;
          // TODO for now, more elegant login error handling needed
          this.errorCode = error.status;
          if (error.status === ResponseCode.NUMBER_401) {
            this.errorMessage = 'Nieprawidłowy login i/lub hasło';
          }
          this.eventEmitter.error(error);
        }
      );
    }
  }

  public ngOnInit(): any {
    this.loginForm = this.fb.group({
      user: [this._user, Validators.required],
      password: [this._password, Validators.required],
    });
  }
}
