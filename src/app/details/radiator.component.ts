import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '../app.service';
import { AuthService } from '../services/auth.service';
import { EventEmitterService } from '../services/event-emitter.service';
import { DefaultService, Radiator } from '../api';

@Component({
  styleUrls: ['./radiator.component.scss'],
  templateUrl: './radiator.component.html'
})
export class RadiatorComponent {
  public item: Radiator;
  protected itemId: number;

  constructor(private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private appState: AppState,
              private eventEmitter: EventEmitterService) {
    this.itemId = activatedRoute.snapshot.params['itemId'];
    this.item = appState.get('item');
    if (!this.item.hasOwnProperty('modelName') || this.item.nid !== this.itemId) {
      this.API.radiatorsLangItemNidGetWithHttpInfo(appState.get('lang'), this.itemId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log('radiatorsLangItemNid', data);
            this.item = data.json();
            this.appState.set('item', this.item);
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    }
  }
}
