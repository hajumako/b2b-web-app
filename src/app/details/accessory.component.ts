import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '../app.service';
import { AuthService } from '../services/auth.service';
import { EventEmitterService } from '../services/event-emitter.service';
import { DefaultService, Accessory } from '../api';

@Component({
  styleUrls: ['./accessory.component.scss'],
  templateUrl: './accessory.component.html'
})
export class AccessoryComponent {
  public title: string = 'Akcesorium grzewcze';
  public item: Accessory;
  protected itemId: number;

  constructor(private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private appState: AppState,
              private eventEmitter: EventEmitterService) {
    this.itemId = activatedRoute.snapshot.params['itemId'];
    this.item = appState.get('item');
    if (this.item === null) {
      this.API.accessoriesLangItemNidGetWithHttpInfo(appState.get('lang'), this.itemId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log(data);
            this.item = data.json();
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    }
  }
}
