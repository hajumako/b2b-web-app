import { Component } from '@angular/core';
import { AppState } from '../app.service';
import { AuthService } from '../services/auth.service';
import { EventEmitterService } from '../services/event-emitter.service';
import { ActivatedRoute } from '@angular/router';
import { DefaultService,  Heater } from '../api';

@Component({
  styleUrls: ['./heater.component.scss'],
  templateUrl: './heater.component.html'
})
export class HeaterComponent {
  public title: string = 'Grzałka';
  public item: Heater;
  protected itemId: number;

  constructor(private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private appState: AppState,
              private eventEmitter: EventEmitterService) {
    this.itemId = activatedRoute.snapshot.params['itemId'];
    this.item = appState.get('item');
    console.log(this.item);
    if (this.item === null) {
      this.API.heatersLangItemNidGetWithHttpInfo(appState.get('lang'), this.itemId, AuthService.getRequestParams())
        .subscribe(
          data => {
            console.log(data);
            this.item = data.json();
          },
          error => {
            this.eventEmitter.error(error);
          }
        );
    }
  }
}
