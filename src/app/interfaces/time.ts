export interface Time {
  minutes?: string;

  seconds?: string;
}
