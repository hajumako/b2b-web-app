import { Component, InjectionToken, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { Time } from './interfaces/time';
import { AppState } from './app.service';
import { ConfiguratorService } from './services/configurator.service';
import { EventEmitterService } from './services/event-emitter.service';
import { CartService } from './services/cart.service';
import { CacheService } from './services/cache.service';
import { AuthService } from './services/auth.service';
import { DefaultService, ResponseCode } from './api';
import { Router } from '@angular/router';
import { InfoModel } from './models/info.model';

declare let $: any;

const WEBP = new InjectionToken<string>('webp_supported');

@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  public tokenTime: Time = {
    minutes: '00',
    seconds: '00'
  };
  public sessionTime: Time = {
    minutes: '00',
    seconds: '00'
  };
  public userName: string;
  public isLoggedIn: boolean = false;
  public error: Response;
  public info: InfoModel;
  public errorModalId: string = 'errorModal';
  public infoModalId: string = 'infoModal';
  public expanded: boolean[] = [];

  private _alive: boolean = true;

  constructor(public appState: AppState,
              public API: DefaultService,
              public cartService: CartService,
              private configuratorService: ConfiguratorService,
              private authService: AuthService,
              private cacheService: CacheService,
              private router: Router,
              private location: Location,
              private eventEmitter: EventEmitterService) {
    appState.set('lang', 'pl');
    appState.set('limit', 10);

    location.subscribe(
      data => {
        this.eventEmitter.emitEvent('backNav');
      }
    );

    authService.isLoggedInObs
      .takeWhile(() => this._alive)
      .subscribe(
        loggedIn => {
          this.isLoggedIn = loggedIn;
          this.userName = this.authService.getUserName();
          if (loggedIn) {

          }
        }
      );

    authService.tokenTimer
      .takeWhile(() => this._alive)
      .subscribe(
        tick => {
          this.tokenTime = this.updateTimer(tick);
        }
      );

    authService.sessionTimer
      .takeWhile(() => this._alive)
      .subscribe(
        tick => {
          this.sessionTime = this.updateTimer(tick);
        }
      );

    eventEmitter.event
      .takeWhile(() => this._alive)
      .filter(code => code && EventEmitterService.isJsonString(code) && JSON.parse(code).hasOwnProperty('error'))
      .subscribe(
        event => {
          this.error = <Response> JSON.parse(event).error;
          if (this.error.status === ResponseCode.NUMBER_401) {
            this.authService.logout();
          } else {
            $('#' + this.errorModalId).foundation('open');
          }
        }
      );

    eventEmitter.event
      .takeWhile(() => this._alive)
      .filter(code => code && EventEmitterService.isJsonString(code) && JSON.parse(code).hasOwnProperty('info'))
      .subscribe(
        event => {
          this.info = JSON.parse(event).info;
          $('#' + this.infoModalId).foundation('open');
          this.router.navigate([this.info.redirect]);
          /*this.error = <Response> JSON.parse(event).error;
          if (this.error.status === ResponseCode.NUMBER_401) {
            this.authService.logout();
          } else {
            $('#' + this.modalId).foundation('open');
          }*/
        }
      );
    configuratorService.getColorGroups();
    cartService.getCountries();
  }

  public onLogout() {
    this.authService.logout();
  }

  public ngOnInit() {
    $(document).foundation();
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  private updateTimer(tick: number): Time {
    let time: Time = {
      minutes: '00',
      seconds: '00'
    };
    if (tick > -1) {
      let _m = Math.floor(tick / 60);
      let _s = Math.floor(tick % 60);
      time.minutes = (_m < 10 ? '0' : '') + _m;
      time.seconds = (_s < 10 ? '0' : '') + _s;
    }
    return time;
  }
}
