import { Component, OnDestroy } from '@angular/core';
import { EventEmitterService } from '../services/event-emitter.service';

@Component({
  styleUrls: ['./order.component.scss'],
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnDestroy {
  public title = 'Zamówienie';
  public currentStep: number = 0;

  protected alive: boolean = true;

  constructor(private eventEmitter: EventEmitterService) {
    eventEmitter.event
      .takeWhile(() => this.alive)
      .filter(code => code && EventEmitterService.isJsonString(code) && JSON.parse(code).hasOwnProperty('currentStep'))
      .subscribe(
        event => {
          this.currentStep = JSON.parse(event).currentStep;
        }
      );
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
