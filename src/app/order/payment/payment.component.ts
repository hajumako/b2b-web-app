import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CartService } from '../../services/cart.service';
import { EventEmitterService } from '../../services/event-emitter.service';

const STEP = 1;

@Component({
  styleUrls: ['./payment.component.scss'],
  templateUrl: './payment.component.html'
})
export class PaymentComponent implements OnInit {

  public paymentForm: FormGroup;

  constructor(private fb: FormBuilder,
              private cartService: CartService,
              private eventEmitter: EventEmitterService) {
    this.eventEmitter.emitEvent(JSON.stringify({currentStep: STEP}));
  }

  public onPaymentFormSubmit(): void {
    console.log('submit payment form');
    if (this.paymentForm.valid) {
      this.cartService.setPaymentMethod(this.paymentForm.value.transactionId, this.paymentForm.value.paymentMethod);
    }
  }

  public ngOnInit() {
    this.buildForms();
  }

  private buildForms() {
    this.paymentForm = this.fb.group({
      transactionId: [this.cartService.getPayment().transactionId],
      paymentMethod: [this.cartService.getPayment().paymentMethod, Validators.required]
    });
  }
}
