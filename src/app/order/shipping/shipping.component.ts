import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import { AuthService } from '../../services/auth.service';
import { CartService } from '../../services/cart.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { DefaultService, ResponseCode, Address, AddressTypeInterface } from '../../api';

declare let $: any;

const STEP = 0;

@Component({
  styleUrls: ['./shipping.component.scss'],
  templateUrl: './shipping.component.html'
})
export class ShippingComponent implements OnInit, AfterViewInit {
  public shippingForm: FormGroup;
  public deliveryAddressFG: FormGroup;
  public invoiceAddressFG: FormGroup;
  public saveAddressFC: FormControl;
  public sameInvoiceAddressFC: FormControl;

  public axAddresses: Address[];
  public chosenAddress: number = -1;
  public showAddressesModal: boolean = false;
  public showInvoiceAddressForm: boolean = true;

  public modalId: string = 'addressModal';

  private _cartId: number;

  private _deliveryAddressIdAX: number = 0;
  private _deliveryFieldsUntouched: boolean = false;
  private _pristineValueOfDeliveryAddress;

  private _invoiceAddressIdAX: number = 0;
  private _invoiceFieldsUntouched: boolean = false;
  private _lastValueOfInvoiceAddress;
  private _pristineValueOfInvoiceAddress;
  private _fragment: string = null;

  constructor(public cartService: CartService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private API: DefaultService,
              private authService: AuthService,
              private eventEmitter: EventEmitterService) {
    this.eventEmitter.emitEvent(JSON.stringify({currentStep: STEP}));
    this._cartId = cartService.getCart().orderId;
    // get available addresses from AX
    this.API.accountAxAddressesGetAddressTypeGetWithHttpInfo('delivery', AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountAxAddressesGetAddressType', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.axAddresses = <Address[]> data.json();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public ngOnInit(): any {
    $(document).foundation();
    this.buildForms();
    this.route.fragment.subscribe(fragment => {
      this._fragment = fragment;
    });
  }

  public ngAfterViewInit(): void {
    try {
      if (this._fragment) {
        document.querySelector('#' + this._fragment).scrollIntoView();
      }
    } catch (e) {
      console.log('ShippingComponent error', e);
    }
  }

  public openModal() {
    $('#' + this.modalId).foundation('open');
  }

  public onShippingFormSubmit(): void {
    let delivery = this.deliveryAddressFG.get('addressFields').value;
    delivery.addressIdAX = this.deliveryAddressFG.get('addressIdAX').value;
    delivery.profileId = this.deliveryAddressFG.get('profileId').value;
    delivery.type = this.deliveryAddressFG.get('type').value;
    let invoice = this.invoiceAddressFG.get('addressFields').value;
    invoice.addressIdAX = this.invoiceAddressFG.get('addressIdAX').value;
    invoice.profileId = this.invoiceAddressFG.get('profileId').value;
    invoice.type = this.invoiceAddressFG.get('type').value;
    if (this.shippingForm.valid) {
      this.cartService.setAddresses([
        delivery,
        invoice
      ]);
      this.cartService.setDeliveryType(this.shippingForm.value['deliveryType']);
      this.cartService.setAdditionalData({
        saveAddress: this.shippingForm.value['saveAddress'],
        sameInvoiceAddress: this.shippingForm.value['sameInvoiceAddress']
      });
    }
  }

  public onChooseAddress(): void {
    this.showAddressesModal = false;
    this._deliveryFieldsUntouched = true;
    let ad = this.axAddresses[this.chosenAddress];
    this._deliveryAddressIdAX = ad.addressIdAX;
    this._pristineValueOfDeliveryAddress = {
      organisationName: ad.organisationName,
      nameLine: ad.nameLine,
      thoroughfare: ad.thoroughfare,
      premise: ad.premise,
      postalCode: ad.postalCode,
      locality: ad.locality,
      country: ad.country
    };
    this.deliveryAddressFG.patchValue({
      addressIdAX: this._deliveryAddressIdAX,
      profileId: ad.profileId,
      type: AddressTypeInterface.Shipping,
      addressFields: this._pristineValueOfDeliveryAddress
    });
    this.saveAddressFC.disable();
    this.shippingForm.patchValue({saveAddress: false});
    this.onChangeSameInvoiceAddress(null);
  }

  public onChangeSaveAddress(e): void {
    if (this.shippingForm.get('saveAddress').value) {
      this.deliveryAddressFG.patchValue({addressIdAX: -1});
    } else if (this._deliveryFieldsUntouched) {
      this.deliveryAddressFG.patchValue({addressIdAX: this._deliveryAddressIdAX});
    } else {
      this.deliveryAddressFG.patchValue({addressIdAX: 0});
    }
  }

  public onChangeSameInvoiceAddress(e): void {
    this.showInvoiceAddressForm = !this.shippingForm.get('sameInvoiceAddress').value;
    if (this.shippingForm.get('sameInvoiceAddress').value) {
      this._lastValueOfInvoiceAddress = this.invoiceAddressFG.get('addressFields').value;
      this.invoiceAddressFG.patchValue({
        addressIdAX: this.deliveryAddressFG.get('addressIdAX').value,
        addressFields: this.deliveryAddressFG.get('addressFields').value
      });
    } else {
      this.invoiceAddressFG.patchValue({
        addressFields: this._lastValueOfInvoiceAddress || this._pristineValueOfInvoiceAddress
      });
    }
  }

  private buildForms(): void {
    let val = this.cartService.getAdditionalData('saveAddress');
    console.log('saveAddress', val);
    this.saveAddressFC = this.fb.control(
      val || false, Validators.required
    );
    val = this.cartService.getAdditionalData('sameInvoiceAddress');
    console.log('sameInvoiceAddress', val);
    this.sameInvoiceAddressFC = this.fb.control(
      val || false, Validators.required
    );
    this.cartService.getCart().addresses.forEach(ad => {
      let adFG = this.fb.group({
        addressIdAX: [ad.addressIdAX],
        profileId: [ad.profileId],
        type: [ad.type, Validators.required],
        addressFields: this.fb.group({
          organisationName: [ad.organisationName, Validators.required],
          nameLine: [ad.nameLine, Validators.required],
          thoroughfare: [ad.thoroughfare, Validators.required],
          premise: [ad.premise, Validators.required],
          postalCode: [ad.postalCode, Validators.required],
          locality: [ad.locality, Validators.required],
          country: [ad.country, Validators.required]
        })
      });
      if (ad.type === AddressTypeInterface.Shipping) {
        this.deliveryAddressFG = adFG;
        this._deliveryAddressIdAX = ad.addressIdAX;
        this._pristineValueOfDeliveryAddress = adFG.get('addressFields').value;
        if (ad.addressIdAX > 0) {
          this.saveAddressFC.disable();
        }
      } else if (ad.type === AddressTypeInterface.Billing) {
        this.invoiceAddressFG = adFG;
        this._invoiceAddressIdAX = ad.addressIdAX;
        this._pristineValueOfInvoiceAddress = adFG.get('addressFields').value;
      }
    });
    if (this.deliveryAddressFG == null || this.invoiceAddressFG == null) {
      let emptyGroup = this.fb.group({
        addressIdAX: [''],
        profileId: [''],
        type: ['', Validators.required],
        addressFields: this.fb.group({
          organisationName: ['', Validators.required],
          nameLine: ['', Validators.required],
          thoroughfare: ['', Validators.required],
          premise: ['', Validators.required],
          postalCode: ['', Validators.required],
          locality: ['', Validators.required],
          country: ['', Validators.required]
        })
      });
      if (this.deliveryAddressFG == null) {
        emptyGroup.patchValue({type: AddressTypeInterface.Shipping});
        this.deliveryAddressFG = emptyGroup;
      }
      if (this.invoiceAddressFG == null) {
        emptyGroup.patchValue({type: AddressTypeInterface.Billing});
        this.invoiceAddressFG = emptyGroup;
      }
    }
    this.deliveryAddressFG.get('addressFields').valueChanges
      .debounceTime(100)
      .subscribe(
        data => {
          this._deliveryFieldsUntouched = JSON.stringify(data) === JSON.stringify(this._pristineValueOfDeliveryAddress);
          if (this._deliveryFieldsUntouched) {
            // set address AX id if fields were untouched
            this.deliveryAddressFG.patchValue({addressIdAX: this._deliveryAddressIdAX});
            // disable "zapisz jako nowy adres" if address fields were untouched
            this.saveAddressFC.disable();
          } else {
            // reset address AX id if fields were touched
            this.deliveryAddressFG.patchValue({addressIdAX: 0});
            // enable "zapisz jako nowy adres" if address fields were touched
            this.saveAddressFC.enable();
          }
          if (this.shippingForm.get('sameInvoiceAddress').value) {
            this.invoiceAddressFG.patchValue({
              addressIdAX: this.deliveryAddressFG.get('addressIdAX').value,
              addressFields: this.deliveryAddressFG.get('addressFields').value
            });
          }
        }
      );
    this.invoiceAddressFG.get('addressFields').valueChanges
      .debounceTime(100)
      .subscribe(
        data => {
          if (!this.shippingForm.get('sameInvoiceAddress').value) {
            this._invoiceFieldsUntouched = JSON.stringify(data) === JSON.stringify(this._pristineValueOfInvoiceAddress);
            if (this._invoiceFieldsUntouched) {
              // set address AX id if fields were untouched
              this.invoiceAddressFG.patchValue({addressIdAX: this._invoiceAddressIdAX});
            } else {
              // reset address AX id if fields were touched
              this.invoiceAddressFG.patchValue({addressIdAX: 0});
            }
          }
        }
      );
    this.shippingForm = this.fb.group({
      deliveryAddress: this.deliveryAddressFG,
      saveAddress: this.saveAddressFC,
      sameInvoiceAddress: this.sameInvoiceAddressFC,
      invoiceAddress: this.invoiceAddressFG,
      deliveryType: [this.cartService.getCart().deliveryType, Validators.required]
    });
  }
}
