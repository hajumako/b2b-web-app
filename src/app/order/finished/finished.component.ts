import { Component, OnDestroy } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { AuthService } from '../../services/auth.service';
import { OrderStatusInterface, DefaultService, ResponseCode, PaymentGateway, Order } from '../../api';

const TITLES = {
  PROCESSING: 'Przetwarzam',
  SENT: 'Zamówienie zostało wysłane',
  SAVED: 'Zamówienie zapisane, problem z dodaniem do AX',
  REDIRECT: 'Za chwilę nastąpi przekierowanie do PayU'
};

const STEP = 3;
const REDIRECT_TIMEOUT = 3000;

@Component({
  styleUrls: ['./finished.component.scss'],
  templateUrl: './finished.component.html'
})
export class FinishedComponent implements OnDestroy {
  public title: string = TITLES.PROCESSING;
  public order: Order;
  public paymentGateway: PaymentGateway;
  public loading: boolean = true;
  public state: number = 0;

  protected _alive: boolean = true;

  constructor(private cartService: CartService,
              private eventEmitter: EventEmitterService,
              private authService: AuthService,
              private API: DefaultService) {
    eventEmitter.emitEvent(JSON.stringify({currentStep: STEP}));
    eventEmitter.event
      .takeWhile(() => this._alive)
      .filter(code => code && EventEmitterService.isJsonString(code) && JSON.parse(code).hasOwnProperty('orderStatusUpdated'))
      .subscribe(
        event => {
          console.log(event);
          let orderStatus = JSON.parse(event).orderStatusUpdated;
          if (orderStatus === OrderStatusInterface.Invoiced) {
            // on delivery payment
            this.title = TITLES.SENT;
            this.state = 1;
            this.getOrder();
          } else if (orderStatus === OrderStatusInterface.AxProcessing) {
            // on delivery payment; failed to save to AX
            this.title = TITLES.SAVED;
            this.state = 2;
            this.loading = false;
          } else if (orderStatus === OrderStatusInterface.B2bSaved) {
            // payU payment
            this.title = TITLES.REDIRECT;
            this.state = 3;
            this.getPayment();
          }
        }
      );
  }

  public ngOnDestroy() {
    this._alive = false;
  }

  private getOrder() {
    this.API.accountOrdersItemOrderIdGetWithHttpInfo(this.cartService.getCart().orderId, null, null, null, null, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountOrdersItemOrderId', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.order = <Order> data.json();
            this.loading = false;
          }
        },
        error => {
          console.log('err', error);
          if (error.status === ResponseCode.NUMBER_401) {
            this.authService.logout();
          }
        }
      );
  }

  private getPayment() {
    this.API.paymentOrdersOrderIdPostWithHttpInfo(this.cartService.getCart().orderId, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('paymentOrdersOrderId', data);
          if (data.status === ResponseCode.NUMBER_200) {
            this.paymentGateway = <PaymentGateway> data.json();
            setTimeout(() => {
              window.location.href = this.paymentGateway.redirectUri;
            }, REDIRECT_TIMEOUT);
            this.loading = false;
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }
}
