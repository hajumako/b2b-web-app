import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CartService } from '../../services/cart.service';
import { AuthService } from '../../services/auth.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { Order, OrderStatusInterface, DefaultService, ResponseCode, Payment } from '../../api';

const TITLES = {
  PROCESSING: 'Przetwarzam',
  PAYMENT: 'Twoja płatność jest przetwarzana'
};

const STEP = 4;

@Component({
  styleUrls: ['./continue-url.component.scss'],
  templateUrl: './continue-url.component.html'
})
export class ContinueUrlComponent implements OnDestroy {
  public title: string = TITLES.PROCESSING;
  public order: Order;
  public payment: Payment;
  public loading: boolean = false;

  protected alive: boolean = true;

  constructor(private cartService: CartService,
              private activatedRoute: ActivatedRoute,
              private authService: AuthService,
              private API: DefaultService,
              private router: Router,
              private eventEmitter: EventEmitterService) {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          let error = params['error'];
          if (params.hasOwnProperty('error')) {
            if (params['error'] === 501) {
              // TODO handle transaction error
            }
          } else {
            // check if navigated to page from payU, otherwise redirect to main page
            if ([OrderStatusInterface.Pending].indexOf(this.cartService.getCart().status) > -1) {
              this.getPayment(this.cartService.getCart().orderId);
              this.getOrder(this.cartService.getCart().orderId);
              this.cartService.refreshCart();
              this.loading = false;
            } else {
              router.navigateByUrl('/dashboard');
            }
          }
        }
      );
  }

  public ngOnDestroy() {
    this.alive = false;
  }

  private getPayment(orderId: number) {
    this.API.paymentOrdersOrderIdGetWithHttpInfo(orderId, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('paymentOrdersOrderId', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.title = TITLES.PAYMENT;
            this.payment = <Payment> data.json();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private getOrder(orderId: number) {
    this.API.accountOrdersItemOrderIdGetWithHttpInfo(orderId, null, null, null, null, AuthService.getRequestParams())
      .subscribe(
        data => {
          console.log('accountOrdersItemOrderId', data);
          this.authService.checkTokenHeader(data.headers);
          if (data.status === ResponseCode.NUMBER_200) {
            this.order = <Order> data.json();
          }
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }
}
