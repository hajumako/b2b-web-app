import { AuthGuard } from '../shared/auth.guard';
import { OrderComponent } from './order.component';
import { PaymentComponent } from './payment/payment.component';
import { ShippingComponent } from './shipping/shipping.component';
import { SummaryComponent } from './summary/summary.component';
import { FinishedComponent } from './finished/finished.component';
import { ContinueUrlComponent } from './continue-url/continue-url.component';

export const routes = [
  {
    path: '', component: OrderComponent, children: [
    {path: '', pathMatch: 'full', redirectTo: 'shipping'},
    {path: 'shipping', component: ShippingComponent, canActivate: [AuthGuard]},
    {path: 'payment', component: PaymentComponent, canActivate: [AuthGuard]},
    {path: 'summary', component: SummaryComponent, canActivate: [AuthGuard]},
    {path: 'finished', component: FinishedComponent, canActivate: [AuthGuard]},
    {path: 'continue', component: ContinueUrlComponent, canActivate: [AuthGuard]}
  ]
  },
];
