import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { OrderStatusInterface } from '../../api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const STEP = 2;

@Component({
  styleUrls: ['./summary.component.scss'],
  templateUrl: './summary.component.html'
})
export class SummaryComponent implements OnInit {
  public configs: Object;
  public summaryForm: FormGroup;

  protected _alive: boolean = true;

  constructor(public cartService: CartService,
              private fb: FormBuilder,
              private eventEmitter: EventEmitterService) {
    eventEmitter.emitEvent(JSON.stringify({currentStep: STEP}));

    this.cartService.getConfigs(this.cartService.getCartSkus())
      .subscribe(
        data => {
          this.configs = data;
          console.log('configs:', this.configs);
        }
      );
  }

  public ngOnInit(): any {
    this.buildForms();
  }
  
  public onPlaceOrder() {
    if (this.summaryForm.valid) {
      this.cartService.setAdditionalData({
        personalDataAgreement: this.summaryForm.value['personalDataAgreement']
      });
      this.cartService.updateOrderStatus(OrderStatusInterface.B2bSaved);
    }
  }

  private buildForms(): void {
    let val = this.cartService.getAdditionalData('personalDataAgreement');
    this.summaryForm = this.fb.group({
      personalDataAgreement: [val || false, Validators.requiredTrue]
    });
  }
}
