import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { PaymentMethodInterface, OrderStatusInterface } from '../../api';

@Component({
  selector: 'order-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Output() public nextStepClick = new EventEmitter();
  @Input() public formValid: boolean;
  @Input() public currentStep = 0;

  public prevStepUrl: string = null;
  public nextStepUrl: string = null;
  public btnLabel: string;

  public urls = [
    [
      '/order/shipping'
    ],
    [
      '/order/payment'
    ],
    [
      '/order/summary'
    ],
    [
      '/order/finished',
      '/order/finished'
    ]
  ];

  private btnLabels = [
    [
      'Płatność'
    ],
    [
      'Podsumowanie'
    ],
    [
      'Zamów i zapłać',
      'Zamów'
    ]
  ];

  private orderStatus = [
    OrderStatusInterface.B2bCheckoutShipping,
    OrderStatusInterface.B2bCheckoutPayment,
    OrderStatusInterface.B2bCheckoutReview
  ];

  constructor(private cartService: CartService) {
  }

  public ngOnInit() {
    this.cartService.updateOrderStatus(this.orderStatus[this.currentStep]);
    let i = 0;
    if (this.currentStep === 2) {
      if (this.cartService.getPayment().paymentMethod === PaymentMethodInterface.CommerceCod) {
        i = 1;
      }
    }
    if (this.currentStep > 0) {
      this.prevStepUrl = this.urls[this.currentStep - 1][0];
    }
    this.nextStepUrl = this.urls[this.currentStep + 1][i];
    this.btnLabel = this.btnLabels[this.currentStep][i];
  }
}
