import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { routes } from './order.routes';
import { PipesModule } from '../pipes';
import { SharedComponentsModule } from '../shared-components';
import { OrderComponent } from './order.component';
import { ShippingComponent } from './shipping/shipping.component';
import { PaymentComponent } from './payment/payment.component';
import { SummaryComponent } from './summary/summary.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FinishedComponent } from './finished/finished.component';
import { ContinueUrlComponent } from './continue-url/continue-url.component';
import { _DevComponentsModule } from '../_dev-components/_dev-components.module';

@NgModule({
  declarations: [
    OrderComponent,
    NavigationComponent,
    ShippingComponent,
    PaymentComponent,
    SummaryComponent,
    FinishedComponent,
    ContinueUrlComponent
  ],
  exports: [],
  imports: [
    PipesModule,
    SharedComponentsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
    _DevComponentsModule
  ],
})
export class OrderModule {
  public static routes = routes;
}
