import { Pipe, PipeTransform } from '@angular/core';
import { CartService } from '../services/cart.service';

@Pipe({
  name: 'prepareId'
})
export class PrepareIdPipe implements PipeTransform {
  constructor(private cartService: CartService) {
  }

  public transform(value: string, revert: boolean = false): string {
    if (revert) {
      return value.replace(/_/g, '/');
    }
    return value.replace(/\//g, '_');
  }
}
