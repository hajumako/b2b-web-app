import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyFormat'
})
export class CurrencyFormat implements PipeTransform {
  public transform(value: number,
                   currencySign: string = ' zł',
                   decimalLength: number = 2,
                   chunkDelimiter: string = '\xa0',  // &nbsp;
                   decimalDelimiter: string = ',',
                   chunkLength: number = 3): string {

    if (value === 0) {
      return 'brak ceny';
    }
    value /= 100;

    let result = '\\d(?=(\\d{' + chunkLength + '})+' + (decimalLength > 0 ? '\\D' : '$') + ')';
    let num = value.toFixed(Math.max(0, ~~decimalLength));

    return (decimalDelimiter ? num.replace('.', decimalDelimiter) : num).replace(new RegExp(result, 'g'), '$&' + chunkDelimiter) + currencySign;
  }
}
