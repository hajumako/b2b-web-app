import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ralPipe'
})
export class RalPipe implements PipeTransform {
  public transform(value: any): string {
    if (value.toString().length === 3) {
      return value.toString().substring(0, 1) + '0' + value.toString().substring(1);
    }
    return value;
  }
}
