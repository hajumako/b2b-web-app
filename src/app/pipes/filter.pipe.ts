import { Pipe, PipeTransform } from '@angular/core';
import { WarehouseItem } from '../api/model/warehouseItem';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {
  public transform(items: any[], field: string, value: string): any {
    if (!items || !field || !value || value === '0') {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter(item => item[field] === value);
  }
}
