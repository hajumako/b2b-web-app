import { NgModule } from '@angular/core';
import { CurrencyFormat } from './currency.pipe';
import { FilterPipe } from './filter.pipe';
import { DimensionsPipe } from './dimensions.pipe';
import { CountryPipe } from './country.pipe';
import { RalPipe } from './ral.pipe';
import { PrepareIdPipe } from './prepareid.pipe';

@NgModule({
  declarations: [
    CurrencyFormat,
    FilterPipe,
    DimensionsPipe,
    CountryPipe,
    RalPipe,
    PrepareIdPipe
  ],
  exports: [
    CurrencyFormat,
    FilterPipe,
    DimensionsPipe,
    CountryPipe,
    RalPipe,
    PrepareIdPipe
  ],
  imports: []
})
export class PipesModule {
}
