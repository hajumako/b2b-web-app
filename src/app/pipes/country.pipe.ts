import { Pipe, PipeTransform } from '@angular/core';
import { CartService } from '../services/cart.service';

@Pipe({
  name: 'countryName'
})
export class CountryPipe implements PipeTransform {
  constructor(private cartService: CartService) {
  }

  public transform(value: string): string {
    if (this.cartService.countries) {
      return this.cartService.countries.find(
        country => country.countryRegion === value
      ).shortName;
    } else {
      return value;
    }
  }
}
