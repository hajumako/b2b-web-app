import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dimensionsPipe'
})
export class DimensionsPipe implements PipeTransform {
  public transform(value: string,
                   dimType: string = 'width',
                   separator: string = '/',
                   unit: string = 'mm'): string {
    let pattern = '(\\S)+';
    let x = value.match(new RegExp(pattern, 'g'));

    if (x && x.length) {
      let dims = x[x.length - 1].split(separator);

      if (dimType === 'height') {
        return dims[0] + unit;
      } else if (dimType === 'width') {
        return dims[1] + unit;
      }
    }
    return '?';
  }
}
