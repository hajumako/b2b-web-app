import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventEmitterService } from '../../services/event-emitter.service';
import { CacheService } from '../../services/cache.service';
import { CartService } from '../../services/cart.service';
import { ParserService } from '../../services/parser.service';
import { StepModel } from '../../models/step.model';
import { CachedComponent } from '../../shared/cached.component';
import { InventConfigurationStateTER, InventConfigurationStateStructTER } from '../../api';

const STEP = 1;

@Component({
  styleUrls: ['./confirm.component.scss'],
  templateUrl: './confirm.component.html'
})
export class ConfirmComponent extends CachedComponent implements OnInit {
  public invalidCodesForm: FormGroup;
  public validCodesForm: FormGroup;
  public configs: Object;
  public prices: Object;
  public counter: number[] = [];

  public invalidBusy: boolean = false;

  public validList: string[] = [];
  public invalidList: string[] = [];
  public duplicatedList: string[] = [];

  private _loadingConfigs: boolean = false;
  private _loadingPrices: boolean = false;

  get products(): FormArray {
    return this.validCodesForm.get('products') as FormArray;
  }

  constructor(private fb: FormBuilder,
              private cartService: CartService,
              private router: Router,
              private parserService: ParserService,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.PARSER,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
  }

  public ngOnInit(): any {
    this.buildForms();
    if (this.backNav) {

    } else {
      this.setCodesLists(this.parserService.getValidationResult());
    }
  }

  public goBack() {
    this.location.back();
  }

  public onInvalidCodesSubmit() {
    if (this.invalidCodesForm.valid && !this.invalidBusy) {
      this.invalidBusy = true;
      this.parserService.validateCodesList(this.invalidCodesForm.get('invalidList').value)
        .subscribe(
          data => {
            this.invalidBusy = false;
            this.updateCodesLists(data);
          },
          error => {
            // emitt event that informs app about error & displays modal
            this.eventEmitter.error(error);
          }
        );
    }
  }

  public onToCartSubmit() {
    console.log(this.validCodesForm.value.products);
    if (this.validCodesForm.valid) {
      this.cartService.addToCartBulk(this.validCodesForm.value.products);
      this.router.navigate(['/cart']);
    }
  }

  public onFastOrderSubmit() {
    if (this.validCodesForm.valid) {
      this.cartService.addToCartBulk(this.validCodesForm.value.products);
      this.cartService.fillWithDefault();
      this.router.navigate(['/order/summary']);
    }
  }

  public removeFromList(i: number) {
    this.validList.splice(i, 1);
    this.products.removeAt(i);
  }

  /*public isLoading(type: string) {
    switch (type) {
      case 'configs':
        return this._loadingConfigs;
      case 'prices':
        return this._loadingPrices;
      default:
        return false;
    }
  }*/

  private buildForms() {
    this.invalidCodesForm = this.fb.group({
      invalidList: ['', Validators.required]
    });
    this.validCodesForm = this.fb.group({
      products: this.fb.array([])
    });
  }

  private addProduct(modelName: string, sku: string, quantity: number, amount: number): FormGroup {
    return this.fb.group({
      modelName: [modelName, Validators.required],
      itemId: [0, Validators.required],
      sku: [sku, Validators.required],
      quantity: [null, Validators.required],
      amount: [amount, Validators.required],
      currencyCode: ['PLN', Validators.required],
      data: [0, Validators.required],
      deleted: [0, Validators.required]
    });
  }

  private addConfigsToForm(data: Object) {
    for (let sku in data) {
      if (data.hasOwnProperty(sku)) {
        this.products.push(this.addProduct(
          data[sku].label,
          data[sku].sku,
          data[sku],
          0
        ));
      }
    }
  }

  private assignPrices() {
    this.products.controls.forEach(product => {
      product.patchValue({amount: this.prices[product.get('sku').value].salesPrice});
    });
  }

  private setCodesLists(validationResult: InventConfigurationStateStructTER[]) {
    this.sortValidationResults(validationResult);
    //this._loadingConfigs = true;
    //this._loadingPrices = true;
    this.cartService.getConfigs(this.validList)
      .subscribe(
        data => {
          this.configs = data;
          this.addConfigsToForm(this.configs);
          //this._loadingConfigs = false;
        }
      );
    this.parserService.getPrices(this.validList)
      .subscribe(
        data => {
          this.prices = data;
          this.assignPrices();
          //this._loadingPrices = false;
        }
      );
  }

  private updateCodesLists(validationResult: InventConfigurationStateStructTER[]) {
    let newCodes = this.sortValidationResults(validationResult);
    //this._loadingPrices = true;
    this.cartService.getConfigs(newCodes)
      .subscribe(
        data => {
          Object.assign(this.configs, data);
          this.addConfigsToForm(data);
          //this._loadingConfigs = false;
        }
      );
    this.parserService.getPrices(newCodes)
      .subscribe(
        data => {
          Object.assign(this.prices, data);
          this.assignPrices();
          //this._loadingPrices = false;
        }
      );
  }

  private sortValidationResults(validationResult: InventConfigurationStateStructTER[]): string[] {
    this.invalidList = [];
    let newValidCodes = [];
    validationResult.forEach(result => {
      let sku = result.itemId + (result.configId ? '-' + result.configId : '');
      if (this.validList.indexOf(sku) > -1 || this.invalidList.indexOf(sku) > -1) {
        this.duplicatedList.push(sku);
      } else {
        if (result.configurationState === InventConfigurationStateTER.Released || result.configurationState === InventConfigurationStateTER.Possible) {
          this.validList.push(sku);
          newValidCodes.push(sku);
          this.counter[sku] = 1;
        } else {
          this.invalidList.push(sku);
        }
      }
    });
    console.log('ii', this.invalidList);
    this.invalidCodesForm.patchValue({invalidList: this.invalidList.join('\n')});
    return newValidCodes;
  }
}
