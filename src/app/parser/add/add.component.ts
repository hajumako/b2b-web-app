import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventEmitterService } from '../../services/event-emitter.service';
import { ParserService } from '../../services/parser.service';
import { CacheService } from '../../services/cache.service';
import { StepModel } from '../../models/step.model';
import { CachedComponent } from '../../shared/cached.component';

const STEP = 0;

@Component({
  styleUrls: ['./add.component.scss'],
  templateUrl: './add.component.html'
})
export class AddComponent extends CachedComponent implements OnInit {
  public codesListForm: FormGroup;

  public busy: boolean = false;

  protected codesList: string;

  private DUMMY = 'WGDLF064058-K524YL,WGTUH039100-K916YL\nWGARH032090-K737YL; WGARH032090-KMGRYP\n\nWGARH032090-KMGRYP,wefwefrf';

  constructor(private fb: FormBuilder,
              private router: Router,
              private parserService: ParserService,
              private location: Location,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    let step: StepModel = {
      type: this.eventEmitter.PARSER,
      step: STEP
    };
    this.eventEmitter.emitEvent(JSON.stringify(step));
    if (this.backNav) {
      this.codesList = this.parserService.getCodesString();
    } else {
      this.codesList = this.DUMMY;
    }
  }

  public ngOnInit(): any {
    this.buildForms();
  }

  public goBack() {
    this.location.back();
  }

  public onCodesListSubmit(): void {
    if (this.codesListForm.valid && !this.busy) {
      this.busy = true;
      this.parserService.setCodesList(this.codesListForm.get('codesList').value)
        .subscribe(
          data => {
            this.busy = false;
            this.router.navigate(['/parser/confirm']);
          },
          error => {
            this.busy = false;
            this.eventEmitter.error(error);
          }
        );
    }
  }

  private buildForms() {
    this.codesListForm = this.fb.group({
      codesList: [this.codesList, Validators.required]
    });
  }
}
