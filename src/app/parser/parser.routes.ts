import { AuthGuard } from '../shared/auth.guard';
import { ParserComponent } from './parser.component';
import { AddComponent } from './add/add.component';
import { ConfirmComponent } from './confirm/confirm.component';

export const routes = [
  {
    path: '', component: ParserComponent, children: [
    {path: '', pathMatch: 'full', redirectTo: 'add'},
    {path: 'add', component: AddComponent, canActivate: [AuthGuard]},
    {path: 'confirm', component: ConfirmComponent, canActivate: [AuthGuard]},
  ]
  },
];
