import { Component, OnDestroy } from '@angular/core';
import { EventEmitterService } from '../services/event-emitter.service';
import { StepModel } from '../models/step.model';

@Component({
  styleUrls: ['./parser.component.scss'],
  templateUrl: './parser.component.html'
})
export class ParserComponent implements OnDestroy {
  public title = 'Zamówienie po kodzie';
  public currentStep: number = -1;

  protected alive: boolean = true;

  constructor(private eventEmitter: EventEmitterService) {
    eventEmitter.event
      .takeWhile(() => this.alive)
      .filter(code => EventEmitterService.isOfType(code, eventEmitter.PARSER))
      .subscribe(
        event => {
          this.currentStep = (<StepModel> JSON.parse(event)).step;
        }
      );
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
