import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../pipes/pipes.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { routes } from './parser.routes';
import { ParserComponent } from './parser.component';
import { AddComponent } from './add/add.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { Autosize } from 'angular2-autosize';

@NgModule({
  declarations: [
    ParserComponent,
    AddComponent,
    ConfirmComponent,
    Autosize
  ],
  exports: [],
  imports: [
    PipesModule,
    SharedComponentsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
})
export class ParserModule {
  public static routes = routes;
}
