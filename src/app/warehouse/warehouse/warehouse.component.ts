import { AfterViewInit, Component, InjectionToken, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AppState } from '../../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CacheService } from '../../services/cache.service';
import { CartService } from '../../services/cart.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService, WarehouseItem, StockModel, WarehouseItemTypeInterface } from '../../api';
import { ResponseCode } from '../../api/model/responseCode';

const CACHE_WAR_R_M = new InjectionToken<string>('war_r_m');
const CACHE_WAR_R = new InjectionToken<string>('war_r');
const CACHE_TOTAL = new InjectionToken<string>('war_r_total');
const CACHE_PAGE = new InjectionToken<string>('war_r_page');
const CACHE_ACTIVE_TAB = new InjectionToken<string>('cache_war_active_tab');

@Component({
  styleUrls: ['./warehouse.component.scss'],
  templateUrl: './warehouse.component.html'
})
export class WarehouseComponent extends CachedComponent implements OnInit, AfterViewInit {
  public title = 'Stany magazynowe (Drupal)';
  public activeTab: number = 0;

  public stockModels: StockModel[];
  public counters: number[];
  public radiators: WarehouseItem[];
  public heaters: WarehouseItem[];
  public accessories: WarehouseItem[];
  public total: number = 0;
  public filterField: string = 'nid';
  public filterValue: string;
  public totalRad: number = 0;
  public currentPageRad: number = 1;
  public filter: string = null;
  public limit: number;

  public productGroups = [
    'Grzejniki',
    'Grzałki',
    'Akcesoria'
  ];

  public _NaN: number = NaN;

  private _order: string = null;
  private _lang: string;
  private _fragment: string = null;

  constructor(private authService: AuthService,
              private appState: AppState,
              private cartService: CartService,
              private route: ActivatedRoute,
              protected API: DefaultService,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    this._lang = appState.get('lang');
    this.limit = appState.get('limit');

    if (this.backNav) {
      this.stockModels = <StockModel[]> cacheService.getCache(CACHE_WAR_R_M.toString());
      this.radiators = <WarehouseItem[]> cacheService.getCache(CACHE_WAR_R.toString());
      this.totalRad = <number> this.cacheService.getCache(CACHE_TOTAL.toString());
      this.currentPageRad = <number> this.cacheService.getCache(CACHE_PAGE.toString());
      this.counters = Array.apply(null, Array(this.radiators.length)).map(() => {
        return 1;
      });
    } else {
      this.API.warehouseLangModelsProductTypeGetWithHttpInfo(
        this.appState.get('lang'),
        WarehouseItemTypeInterface.Radiators.toString(),
        null,
        null,
        AuthService.getRequestParams()
      )
        .subscribe(
          data => {
            this.authService.checkTokenHeader(data.headers);
            if (data.headers.has('X-Total')) {
              this.total = Number(data.headers.get('X-Total'));
            }
            let all: StockModel[] = [
              {nid: 0, modelName: 'Wszystkie modele'}
            ];
            this.stockModels = all.concat(data.json());
            this.cacheService.saveCache(CACHE_WAR_R_M.toString(), this.stockModels);
          },
          error => {
            this.eventEmitter.error(error);
          }
        );

      this.getWarehousePage(this.currentPageRad);
    }
  }

  public ngOnInit(): any {
    this.route.fragment.subscribe(fragment => {
      this._fragment = fragment;
    });
  }

  public ngAfterViewInit(): void {
    try {
      switch (this._fragment) {
        case 'radiators':
          this.activateTab(0);
          break;
        case 'heaters':
          this.activateTab(1);
          break;
        case 'accessories':
          this.activateTab(2);
          break;
        default:
          this.activateTab(0);
      }
    } catch (e) {
      console.log('DashboardComponent error', e);
    }
  }

  public activateTab(i: number) {
    this.activeTab = i;
    this.cacheService.saveCache(CACHE_ACTIVE_TAB.toString(), this.activeTab);
  }

  public getWarehousePage(page: number) {
    this.radiators = null;
    this._order = 'modelName.asc';
    this.API.defaultHeaders = this.authService.getHeaders();
    this.API.warehouseLangListProductTypeGetWithHttpInfo(
      this.appState.get('lang'),
      WarehouseItemTypeInterface.Radiators.toString(),
      this.filter,
      this._order,
      this.limit,
      this.limit * (page - 1),
      AuthService.getRequestParams()
    )
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          this.fillRadiatorData(data, page);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public addToCart(i: number) {
    this.cartService.addToCart({
      modelName: this.radiators[i].modelName,
      itemId: 0,
      sku: this.radiators[i].sku,
      quantity: this.counters[i],
      amount: this.radiators[i].price,
      currencyCode: 'PLN',
      data: '',
      deleted: 0
    });
  }

  public onModelChange(event: Event): void {
    this.radiators = null;
    let v = (event.target as HTMLSelectElement).value;
    this.filter = v === '0' ? null : 'nid.eq.' + v;
    this.API.defaultHeaders = this.authService.getHeaders();
    this.API.warehouseLangListProductTypeGetWithHttpInfo(
      this.appState.get('lang'),
      WarehouseItemTypeInterface.Radiators.toString(),
      this.filter,
      this._order,
      this.limit,
      0,
      AuthService.getRequestParams()
    )
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          this.fillRadiatorData(data, 1);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private fillRadiatorData(data: any, page: number) {
    if (data.status === ResponseCode.NUMBER_200) {
      if (data.headers.has('X-Total')) {
        this.totalRad = Number(data.headers.get('X-Total'));
        this.cacheService.saveCache(CACHE_TOTAL.toString(), this.totalRad);
      }
      this.radiators = <WarehouseItem[]> data.json();
      this.currentPageRad = page;
      this.cacheService.saveCache(CACHE_PAGE.toString(), page);
      this.counters = Array.apply(null, Array(this.radiators.length)).map(() => {
        return 1;
      });
      this.cacheService.saveCache(CACHE_WAR_R.toString(), this.radiators);
    }
  }
}
