import { Component, InjectionToken } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '../../app.service';
import { AuthService } from '../../services/auth.service';
import { DefaultService, WarehouseItem } from '../../api';
import { CacheService } from '../../services/cache.service';
import { Radiator } from '../../api/model/radiator';
import { EventEmitterService } from '../../services/event-emitter.service';
import { RadiatorSpecs } from '../../api/model/radiatorSpecs';
import { CartService } from '../../services/cart.service';
import { ProductConfiguration } from '../../api/model/productConfiguration';
import { InventAvailability } from '../../api/model/inventAvailability';
import { NoYes } from '../../api/model/noYes';

const CACHE_WAR_R = new InjectionToken<string>('war_r');
const CACHE_WAR_R_AX = new InjectionToken<string>('war_r_ax');

@Component({
  styleUrls: ['./warehouseDetails.component.scss'],
  templateUrl: './warehouseDetails.component.html'
})
export class WarehouseDetailsComponent {
  public title = 'Szczegóły';
  public productId: number;
  public sku: string;
  public itemDrupal: WarehouseItem;
  public itemAX: InventAvailability;
  public radiatorConfig: ProductConfiguration;
  public radiatorSpecs: RadiatorSpecs;
  public radiatorData: Radiator;
  public salesPrice: number;
  public listPrice: number;
  public qty: number;
  public outlet: boolean;
  public counter: number = 1;

  constructor(private cartService: CartService,
              private API: DefaultService,
              private activatedRoute: ActivatedRoute,
              private appState: AppState,
              protected cacheService: CacheService,
              protected eventEmitter: EventEmitterService) {
    if (activatedRoute.snapshot.params['itemId'] && activatedRoute.snapshot.params['configId']) {
      this.sku = activatedRoute.snapshot.params['itemId'] + '-' + activatedRoute.snapshot.params['configId'];
      for (let i of <InventAvailability[]> this.cacheService.getCache(CACHE_WAR_R_AX.toString())) {
        if (this.sku === i.itemId + '-' + i.configId) {
          this.itemAX = i;
          this.radiatorConfig = i.config;
          this.salesPrice = this.itemAX.salesPrice;
          this.listPrice = this.itemAX.listPrice;
          this.qty = this.itemAX.qty;
          this.outlet = this.itemAX.isOutlet === NoYes.Yes;
          break;
        }
      }
      console.log('asd',this.radiatorConfig);
      this.getItemAXSpecs(this.itemAX.itemId, this.itemAX.configId);
      this.API.radiatorsLangListGetWithHttpInfo('pl', 'modelAX.eq.ARH', null, null, null, AuthService.getRequestParams())
        .subscribe(
          data => {
            let radiator = <Radiator> data.json()[0];
            console.log(radiator);
            this.getItemDetails(radiator.nid);
          },
          error => {
            this.eventEmitter.error(error);
          });
    } else if (activatedRoute.snapshot.params['productId']) {
      this.productId = parseInt(activatedRoute.snapshot.params['productId'], 10);
      for (let i of <WarehouseItem[]> this.cacheService.getCache(CACHE_WAR_R.toString())) {
        if (i.productId === this.productId) {
          this.itemDrupal = i;
          this.radiatorConfig = i.config;
          this.salesPrice = this.itemDrupal.price;
          this.listPrice = this.itemDrupal.priceRrp;
          this.qty = this.itemDrupal.quantity;
          this.outlet = this.itemDrupal.sale;
          break;
        }
      }
      this.getItemAXSpecs(this.itemDrupal.config.itemId, this.itemDrupal.config.configId);
      this.getItemDetails(this.itemDrupal.nid);
    }
  }

  public addToCart(i: number) {
    this.cartService.addToCart({
      modelName: this.radiatorConfig.label,
      itemId: 0,
      sku: this.radiatorConfig.itemId + '-' + this.radiatorConfig.configId,
      quantity: this.counter,
      amount: this.itemAX[i].salesPrice,
      currencyCode: 'PLN',
      data: '',
      deleted: 0
    });
  }

  private getItemDetails(nid: number) {
    this.API.radiatorsLangItemNidGetWithHttpInfo(this.appState.get('lang'), nid, AuthService.getRequestParams())
      .subscribe(
        data => {
          this.radiatorData = <Radiator> data.json();
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private getItemAXSpecs(itemId: string, configId: string) {
    this.API.radiatorsLangSpecsGetItemIdConfigIdGetWithHttpInfo(this.appState.get('lang'), itemId, configId, AuthService.getRequestParams())
      .subscribe(
        data => {
          this.radiatorSpecs = <RadiatorSpecs> data.json();
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }
}
