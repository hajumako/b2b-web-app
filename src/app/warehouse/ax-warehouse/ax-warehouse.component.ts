import { AfterViewInit, Component, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { AppState } from '../../app.service';
import { CacheService } from '../../services/cache.service';
import { CartService } from '../../services/cart.service';
import { EventEmitterService } from '../../services/event-emitter.service';
import { CachedComponent } from '../../shared/cached.component';
import { DefaultService, ProductLabel } from '../../api';
import { InventAvailability } from '../../api/model/inventAvailability';
import { ProductGroupId } from '../../api/model/productGroupId';
import { ResponseCode } from '../../api/model/responseCode';

const CACHE_WAR_R_M = new InjectionToken<string>('war_r_ax_m');
const CACHE_WAR_R = new InjectionToken<string>('war_r_ax');
const CACHE_TOTAL = new InjectionToken<string>('war_r_ax_total');
const CACHE_PAGE = new InjectionToken<string>('war_r_ax_page');
const CACHE_ACTIVE_TAB = new InjectionToken<string>('cache_war_ax_active_tab');

@Component({
  styleUrls: ['./ax-warehouse.component.scss'],
  templateUrl: './ax-warehouse.component.html'
})
export class AXWarehouseComponent extends CachedComponent implements OnInit, AfterViewInit {
  public title = 'Stany magazynowe (AX)';
  public activeTab: number = 0;

  public stockModels: ProductLabel[];
  public counters: number[];
  public radiators: InventAvailability[];
  public heaters: InventAvailability[];
  public accessories: InventAvailability[];
  public total: number = 0;
  public filterField: string = 'nid';
  public filterValue: string;
  public totalRad: number = 0;
  public currentPageRad: number = 1;
  public filter: string = null;
  public limit: number;

  public productGroups = [
    'Grzejniki',
    'Grzałki',
    'Akcesoria'
  ];

  public _NaN: number = NaN;

  private _lang: string;
  private _fragment: string = null;

  constructor(private authService: AuthService,
              private appState: AppState,
              private cartService: CartService,
              private route: ActivatedRoute,
              protected API: DefaultService,
              protected eventEmitter: EventEmitterService,
              protected cacheService: CacheService) {
    super(eventEmitter, cacheService);
    this._lang = appState.get('lang');
    this.limit = appState.get('limit');
    if (this.backNav) {
      this.stockModels = <string[]> cacheService.getCache(CACHE_WAR_R_M.toString());
      this.radiators = <InventAvailability[]> cacheService.getCache(CACHE_WAR_R.toString());
      this.totalRad = <number> this.cacheService.getCache(CACHE_TOTAL.toString());
      this.currentPageRad = <number> this.cacheService.getCache(CACHE_PAGE.toString());
      this.counters = Array.apply(null, Array(this.radiators.length)).map(() => {
        return 1;
      });
    } else {
      this.API.warehouseLangAxModelsGroupIdGetWithHttpInfo(
        this.appState.get('lang'),
        ProductGroupId.WYRG.toString(),
        AuthService.getRequestParams()
      )
        .subscribe(
          data => {
            this.authService.checkTokenHeader(data.headers);
            let all: ProductLabel[] = [
              {model: '0', label: 'Wszystkie modele'}
            ];
            this.stockModels = all.concat(data.json());
            this.cacheService.saveCache(CACHE_WAR_R_M.toString(), this.stockModels);
          },
          error => {
            this.eventEmitter.error(error);
          }
        );

      this.getWarehousePage(this.currentPageRad);
    }
  }

  public ngOnInit(): any {
    this.route.fragment.subscribe(fragment => {
      this._fragment = fragment;
    });
  }

  public ngAfterViewInit(): void {
    try {
      switch (this._fragment) {
        case 'radiators':
          this.activateTab(0);
          break;
        case 'heaters':
          this.activateTab(1);
          break;
        case 'accessories':
          this.activateTab(2);
          break;
        default:
          this.activateTab(0);
      }
    } catch (e) {
      console.log('DashboardComponent error', e);
    }
  }

  public activateTab(i: number) {
    this.activeTab = i;
    this.cacheService.saveCache(CACHE_ACTIVE_TAB.toString(), this.activeTab);
  }

  public getWarehousePage(page: number) {
    this.radiators = null;
    this.API.defaultHeaders = this.authService.getHeaders();
    this.API.warehouseLangAxListGroupIdGetWithHttpInfo(
      this.appState.get('lang'),
      ProductGroupId.WYRG.toString(),
      this.filter,
      this.limit,
      this.limit * (page - 1),
      AuthService.getRequestParams()
    )
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          this.fillRadiatorData(data, page);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  public addToCart(i: number) {
    this.cartService.addToCart({
      modelName: this.radiators[i].config.label,
      itemId: 0,
      sku: this.radiators[i].itemId + '-' + this.radiators[i].configId,
      quantity: this.counters[i],
      amount: this.radiators[i].salesPrice,
      currencyCode: 'PLN',
      data: '',
      deleted: 0
    });
  }

  public onModelChange(event: Event): void {
    this.radiators = null;
    let v = (event.target as HTMLSelectElement).value;
    this.filter = v === '0' ? null : 'model.eq.' + v;
    this.API.defaultHeaders = this.authService.getHeaders();
    this.API.warehouseLangAxListGroupIdGetWithHttpInfo(
      this.appState.get('lang'),
      ProductGroupId.WYRG.toString(),
      this.filter,
      this.limit,
      0,
      AuthService.getRequestParams()
    )
      .subscribe(
        data => {
          this.authService.checkTokenHeader(data.headers);
          this.fillRadiatorData(data, 1);
        },
        error => {
          this.eventEmitter.error(error);
        }
      );
  }

  private fillRadiatorData(data: any, page: number) {
    if (data.status === ResponseCode.NUMBER_200) {
      if (data.headers.has('X-Total')) {
        this.totalRad = Number(data.headers.get('X-Total'));
        this.cacheService.saveCache(CACHE_TOTAL.toString(), this.totalRad);
      }
      this.radiators = <InventAvailability[]> data.json();
      this.currentPageRad = page;
      this.cacheService.saveCache(CACHE_PAGE.toString(), page);
      this.counters = Array.apply(null, Array(this.radiators.length)).map(() => {
        return 1;
      });
      this.cacheService.saveCache(CACHE_WAR_R.toString(), this.radiators);
    }
  }
}
