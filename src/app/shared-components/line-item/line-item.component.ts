import { Component, EventEmitter, Input, OnChanges, Output, SimpleChange } from '@angular/core';
import { ProductConfiguration } from '../../api/model/productConfiguration';
import { LineItem } from '../../api/model/lineItem';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'line-item',
  styleUrls: ['./line-item.component.scss'],
  templateUrl: './line-item.component.html'
})
export class LineItemComponent {
  @Input() public lineItem: any;
  @Input() public index: number;
  @Input() public config: ProductConfiguration;
  @Input() public fixedQty: boolean = false;
  @Input() public showRemoveButton: boolean = false;
  @Output() public onRemoveClick = new EventEmitter<number>();

  constructor(public cartService: CartService) {

  }

  public remove(index: number): void {
    this.onRemoveClick.emit(index);
  }
}
