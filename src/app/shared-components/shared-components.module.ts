import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComponentHeaderComponent } from './component-header/component-header.component';
import { ItemCounterComponent } from './item-counter/item-counter.component';
import { LazyImageComponent } from './lazy-image/lazy-image.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PagerComponent } from './pager/pager.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductsGalleryComponent } from './products-gallery/products-gallery.component';
import { LineItemComponent } from './line-item/line-item.component';
import { _DevComponentsModule } from '../_dev-components/_dev-components.module';
import { PipesModule } from '../pipes/pipes.module';
import { ConnectionLegendComponent } from './connection-legend/connection-legend.component';

@NgModule({
  declarations: [
    ComponentHeaderComponent,
    ConnectionLegendComponent,
    ItemCounterComponent,
    LazyImageComponent,
    LineItemComponent,
    PagerComponent,
    ProductsGalleryComponent,
    ProductsListComponent
  ],
  exports: [
    ComponentHeaderComponent,
    ConnectionLegendComponent,
    ItemCounterComponent,
    LazyImageComponent,
    LineItemComponent,
    PagerComponent,
    ProductsGalleryComponent,
    ProductsListComponent
  ],
  imports: [
    FormsModule,
    LazyLoadImageModule,
    CommonModule,
    PipesModule,
    _DevComponentsModule
  ]
})
export class SharedComponentsModule {}
