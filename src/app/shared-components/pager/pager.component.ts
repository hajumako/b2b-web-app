import { Component, EventEmitter, Input, OnChanges, Output, SimpleChange } from '@angular/core';

@Component({
  selector: 'pager',
  styleUrls: ['./pager.component.scss'],
  templateUrl: './pager.component.html'
})
export class PagerComponent implements OnChanges {
  public totalPages: number;
  public pages: number[] = [];

  @Input() public total: number;
  @Input() public perPage: number;
  @Input() public currentPage: number = 1;
  @Output() public onPageChange = new EventEmitter<number>();

  public setPage(page: number): void {
    if (page && page >= 1 && page <= this.totalPages && page !== this.currentPage) {
      this.onPageChange.emit(page);
      this.currentPage = page;
      this.fillPages();
    }
  }

  public ngOnChanges(changes: { [propKey: string]: SimpleChange }): void {
    if (changes.hasOwnProperty('total')) {
      this.totalPages = Math.ceil(this.total / this.perPage);
    }
    this.fillPages();
  }

  protected fillPages() {
    let i = 1;
    this.pages = [this.currentPage];
    while (this.pages.length < 7 && i <= this.totalPages) {
      if (this.currentPage - i > 0) {
        this.pages.unshift(this.currentPage - i);
      }
      if (this.currentPage + i <= this.totalPages) {
        this.pages.push(this.currentPage + i);
      }
      i++;
    }
    if (this.pages[0] > 1) {
      this.pages.unshift(null);
    }
    if (this.pages[this.pages.length - 1] < this.totalPages) {
      this.pages.push(null);
    }
  }
}
