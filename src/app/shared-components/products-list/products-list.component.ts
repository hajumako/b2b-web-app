import { Component, EventEmitter, Input, OnChanges, Output, SimpleChange } from '@angular/core';

@Component({
  selector: 'products-list',
  styleUrls: ['./products-list.component.scss'],
  templateUrl: './products-list.component.html'
})
export class ProductsListComponent {
  @Input() public products: Object;
  @Input() public images: Object;
  
  @Output() public onOpenDetails = new EventEmitter<number>();

  public openDetails(index: number): void {
    this.onOpenDetails.emit(index);
  }
}
