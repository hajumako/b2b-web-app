import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { isNumber } from 'util';
import { Image } from '../../api';
import * as _ from 'lodash';

@Component({
  selector: 'lazy-img',
  styleUrls: ['./lazy-image.component.scss'],
  templateUrl: './lazy-image.component.html'
})
export class LazyImageComponent implements OnInit, OnChanges {
  @Input() public url: string;
  @Input() public image: Image;
  @Input() public columns: number[];
  @Input() public loader: string = 'assets/img/loader_2.gif';
  @Input() public missingImage: string = 'assets/img/missing-image.png';
  @Input() public scrollTarget = null;
  @Input() public flex: boolean = false;
  @Input() public classes: string = '';

  public offset = 100;
  public source: string;
  public fallback: string;
  public sizes: string;
  public useSrcset: boolean = USE_SRCSET;

  protected _widths = [
    300,
    600,
    900,
    1024
  ];

  protected _sizes = [
    '(min-width: 40em)',
    '(min-width: 64em)',
    ''
  ];

  protected imgEl: Element;

  public ngOnInit() {
      this.imgEl = document.getElementById('lazy-img');
      if (!this.url && !this.image) {
        console.log('empty url & image', this.url, this.image);
      }
      if (!(this.columns instanceof Array)) {
        console.log(this.columns, ': columns should be an array');
      }
      if (this.image) {
        if (this.useSrcset) {
          if (this.columns) {
            this.source = this.calculateSources().join(',');
            this.sizes = this.calculateSizes().join(',');
          } else {
            this.source = this.image.srcset.join(',');
          }
        } else {
          this.source = this.image.defaultUrl;
        }
        this.fallback = this.image.defaultUrl;
      } else if (this.url) {
        this.source = this.url;
        this.fallback = this.url;
      }
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (this.imgEl) {
      this.imgEl.className = this.imgEl.className.replace(/ng-failed-lazyloaded/g, '');
    }
    if (changes.hasOwnProperty('image') && this.image) {
      if (this.useSrcset) {
        if (this.columns) {
          this.source = this.calculateSources().join(',');
        } else {
          this.source = this.image.srcset.join(',');
        }
      } else {
        this.source = this.image.defaultUrl;
      }
      this.fallback = this.image.defaultUrl;
    } else if (changes.hasOwnProperty('url')) {
      this.source = this.url;
      this.fallback = this.url;
    }

    if (changes.hasOwnProperty('columns')) {
      this.sizes = this.calculateSizes().join(',');
    }
  }

  private calculateSources(): any[] {
    let sources = [];
    this._widths.forEach((val) => {
      sources.push(this.image.originalUrl + '/' + val + ' ' + val + 'w');
    });
    return sources;
  }

  private calculateSizes(): any[] {
    let cols = _.clone(this.columns);
    cols.push(cols.shift());
    let sizes = [];
    cols.forEach((val, i) => {
      if (isNumber(val) && val > 0) {
        sizes.push(this._sizes[i] + ' ' + Math.round(cols[i] / 12 * 100) + 'vw');
      }
    });
    return sizes;
  }
}
