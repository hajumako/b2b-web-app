import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'item-counter',
  styleUrls: ['./item-counter.component.scss'],
  templateUrl: './item-counter.component.html'
})
export class ItemCounterComponent {
  public counterValue = 0;
  @Output() public counterChange = new EventEmitter();

  @Input() public min: number;
  @Input() public max: number = NaN;

  @Input()
  get counter() {
    return this.counterValue;
  }

  set counter(val) {
    this.counterValue = val;
    this.counterChange.emit(this.counterValue);
  }

  public decrement() {
    if (isNaN(this.min) || (!isNaN(this.min) && this.counter > this.min)) {
      this.counter--;
    }
  }

  public increment() {
    if (isNaN(this.max) || (!isNaN(this.max) && this.counter < this.max)) {
      this.counter++;
    }
  }

  public checkValue(e) {
    let val = parseInt(e.target.value, 10);
    if (isNaN(val)) {
      this.counter = 1;
    } else if (!isNaN(this.max) && val > this.max) {
      this.counter = this.max;
    } else if (!isNaN(this.min) && val < this.min) {
      this.counter = this.min;
    }
  }
}
