import { Component, EventEmitter, Input, OnChanges, Output, SimpleChange } from '@angular/core';

@Component({
  selector: 'products-gallery',
  styleUrls: ['./products-gallery.component.scss'],
  templateUrl: './products-gallery.component.html'
})
export class ProductsGalleryComponent {
  @Input()
  set gallery(gallery: string) {
    this.links = gallery.split(';');
    this._gallery = gallery;
  }

  get gallery(): string {
    return this._gallery;
  }

  public links: string[];
  private _gallery: string;

}
