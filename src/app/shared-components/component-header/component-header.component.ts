import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'comp-header',
  styleUrls: ['./component-header.component.scss'],
  templateUrl: './component-header.component.html'
})
export class ComponentHeaderComponent {
  @Input() public title: string;
  @Input() public hideBackBtn: boolean = false;

  constructor(private location: Location) {
  }

  public goBack() {
    this.location.back();
  }
}
